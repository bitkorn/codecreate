## high priority

- [ ] MySQL support for Entity (MappingGetterSetterService) and TableType
- [ ] *Entity set $primaryKey from information_schema
- [ ] chown allapow:www-data (after create files)

```bash
# 
chown -R allapow:www-data .
find . -type d -exec chmod 775 {} \;
find . -type f -exec chmod 664 {} \;
```
- [ ] create also view-file folder for HTML controller

## priority
- [ ] create functions
  - [ ] table add a collection of functions

- [ ] create config
  - [ ] add controllers & service_manager entries
  - [ ] add routes

## low priority

- [x] create Module
  - [ ] put it into /config/modules.config.php
  - [ ] put it into /composer.json
  - [ ] composer dump-autoload
