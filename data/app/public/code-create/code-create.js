$(function () {
    function log(message) {
        $("<div>").text(message).prependTo("#log");
        $("#log").scrollTop(0);
    }

    $('input#form_namespace_module').autocomplete({
        source: 'bitkorn-codecreate-ajax-common-autocomplete-namespace',
        minLength: 1,
        classes: 'w3-grey',
        select: function (event, ui) {
            event.preventDefault();
            // console.log(JSON.stringify(ui.item));
            $('#form_namespace_module').val(ui.item.label);
            $('#form_directory_src').val(ui.item.value);
        },
        focus: function (event, ui) {
            $('#form_namespace_module').val(ui.item.label);
            $('#form_directory_src').val(ui.item.value);
            return false;
        }
    }).autocomplete('instance')._renderItem = function (ul, item) {
        // console.log(JSON.stringify(item));
        // log('label: ' + item.label);
        // log('value: ' + item.value);
        return $('<li>')
            .append('<div class="autocomplete-codecreate"><code>' + item.label + '</code> | ' + item.value + '</div>')
            .appendTo(ul);
    };


    $('input#form_namespace_class').autocomplete({
        source: function(request, response){
            let moduleNs = $('input#form_namespace_module').val();
            // let classNs = $(this).val();

            $.ajax({
                url: '/bitkorn-codecreate-ajax-common-autocomplete-filesystem',
                data: {'moduleNs': moduleNs, 'classNs': request.term},
                success: response,
                error: function (jqXHR, textStatus, errorThrown) {
                    response([]);
                }
            });

        },
        minLength: 1,
        classes: 'w3-grey',
        select: function (event, ui) {
            event.preventDefault();
            // console.log(JSON.stringify(ui.item));
            $('#form_namespace_class').val(ui.item.label);
        },
        focus: function (event, ui) {
            $('#form_namespace_class').val(ui.item.label);
            return false;
        }
    }).autocomplete('instance')._renderItem = function (ul, item) {
        // console.log(JSON.stringify(item));
        // log('label: ' + item.label);
        // log('value: ' + item.value);
        return $('<li>')
            .append('<code class="autocomplete-codecreate">' + item.value + '</code>')
            .appendTo(ul);
    };


    $('input#table_name').autocomplete({
        source: function(request, response){
            let tableName = $('input#table_name').val();
            // let classNs = $(this).val();

            $.ajax({
                url: '/bitkorn-codecreate-ajax-common-autocomplete-tablename',
                data: {'tablename': tableName},
                success: response,
                error: function (jqXHR, textStatus, errorThrown) {
                    response([]);
                }
            });

        },
        minLength: 1,
        classes: 'w3-grey',
        select: function (event, ui) {
            event.preventDefault();
            // console.log(JSON.stringify(ui.item));
            $('#table_name').val(ui.item.label);
        },
        focus: function (event, ui) {
            $('#table_name').val(ui.item.label);
            return false;
        }
    }).autocomplete('instance')._renderItem = function (ul, item) {
        // console.log(JSON.stringify(item));
        // log('label: ' + item.label);
        // log('value: ' + item.value);
        return $('<li>')
            .append('<code class="autocomplete-codecreate">' + item.value + '</code>')
            .appendTo(ul);
    };


    // $(document).on('change', 'input#form_namespace_class', function () {
    //     let moduleNs = $('input#form_namespace_module').val();
    //     let classNs = $(this).val();
    //
    //     $.ajax({
    //         url: 'bitkorn-codecreate-ajax-common-autocomplete-filesystem',
    //         data: {'moduleNs': moduleNs, 'classNs': classNs},
    //         success: function (data, textStatus, jqXHR) {
    //             console.log(JSON.stringify(data));
    //             let lis = '';
    //             for (let i in data) {
    //                 lis += '<li class="ui-menu-item">' + data[i].value + '</li>';
    //                 console.log(data[i].value);
    //             }
    //             $('input#form_namespace_class').after('<ul class="ui-menu ui-widget ui-widget-content ui-autocomplete ui-front">' + lis + '</ul>')
    //
    //
    //         },
    //         error: function (jqXHR, textStatus, errorThrown) {
    //             alert(textStatus + ' - ' + errorThrown);
    //         }
    //     });
    // });

});
