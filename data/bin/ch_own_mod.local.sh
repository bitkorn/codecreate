#!/bin/bash
verz="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd "$verz/../"
chmodFolder=775
chmodFile=664

if [ ! -d "data/log" ]
then
mkdir "data/log"
fi

if [ ! -f "data/log/app.log" ]
then
touch "data/log/app.log"
fi

chown -R allapow:www-data .
find . -type d -exec chmod "$chmodFolder" {} \;
find . -type f -exec chmod "$chmodFile" {} \;

chmod 666 "data/log/app.log"

chmod a+x bin/ch_own_mod.local.sh
chmod a+x bin/tar_gz_with_exclude.sh
chmod a+x bin/emptyLog.sh
