#!/bin/bash
verz="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd "$verz/../../../../../module/"
chmodFolder=775
chmodFile=664

chown -R www-data:www-data .
find . -type d -exec chmod "$chmodFolder" {} \;
find . -type f -exec chmod "$chmodFile" {} \;
