#!/bin/bash
verz="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd "$verz/../../"
tarDir="zf3shop"
outDir="/home/allapow/Downloads"
dateString=`date +%Y%m%d%H%M`
fileNamePost="_ConfigFileService-alias-and-duplicate-ok.tar.gz"
tar -czf "$outDir/zf3shopCodeCreate$dateString$fileNamePost" --exclude=".git" --exclude="vendor" --exclude="nbproject" --exclude=".idea" $tarDir
