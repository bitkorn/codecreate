# Code Create
CodeCreate creates:
- Controller (**REST**, AJAX, **HTML**)
  - Factory
  - module.config.php: controllers->factories
- Table
  - Factory
  - module.config.php: service_manager->factories
- Form
  - Factory
  - module.config.php: service_manager->factories
- Service
  - Factory
  - module.config.php: service_manager->factories

# install instructions
require module \Bitkorn\Trinket

Syntax highlighting with [prism](https://prismjs.com)

Some with jQuery & jQuery UI

## Trial trinket
For Zend way ([laminas/laminas-code](https://github.com/laminas/laminas-code)):
```bash
composer require laminas/laminas-code
```

To generate Config Array ([laminas/laminas-config](https://github.com/laminas/laminas-config/)):
```bash
composer require laminas/laminas-config
```

For Nette way ([nette/php-generator](https://github.com/nette/php-generator),
[doc.nette.org/en/3.0/php-generator](https://doc.nette.org/en/3.0/php-generator)):
```bash
composer require nette/php-generator
```
[doc.nette.org](https://doc.nette.org/en/3.0/php-generator)

# use CodeCreate
To use CodeCreate
- all is [PSR-4](https://www.php-fig.org/psr/psr-4)
- folder structure is ZF3
  - e.g. the Module.php is in /src
- you will have a extra config file
  - **SomeModule/config/codecreate.config.php**
  - file owner must be the webserver user (e.g. www-data)

And in Module.php you will merge the configs:
```php
public function getConfig()
{
    return ArrayUtils::merge(include __DIR__ . '/../config/module.config.php', include __DIR__ . '/../config/codecreate.config.php');
}
```
