<?php


namespace Factory\Table;


use Interop\Container\ContainerInterface;
use Skeleton\SkeletonTable;
use Laminas\ServiceManager\Factory\FactoryInterface;

class SkeletonTableFactory implements FactoryInterface
{

    /**
     * Create a SkeletonTable
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @param  null|array $options
     * @return SkeletonTable
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $table = new SkeletonTable();
        $table->setDbAdapter($container->get('dbDefault'));
        $table->setLogger($container->get('logger'));
        return $table;
    }
}
