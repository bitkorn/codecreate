<?php


namespace Bitkorn\CodeCreate\Factory\Service\Create\Module;


use Bitkorn\CodeCreate\Service\Create\File\ConfigFileService;
use Bitkorn\CodeCreate\Service\Create\Module\CreateModuleService;
use Bitkorn\CodeCreate\Service\Create\Type\CreateTypeService;
use Bitkorn\Trinket\Service\Filesystem\FolderTool;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class CreateModuleServiceFactory implements FactoryInterface
{

    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new CreateModuleService();
        $service->setLogger($container->get('logger'));
        $config = $container->get('config');
        $service->setFolderTool($container->get(FolderTool::class));
        $service->setModuleRootPath($config['bitkorn_codecreate']['module_root']);
        $service->setDraftFolder($config['bitkorn_codecreate']['draft_folder']);
        $service->setConfigFileService($container->get(ConfigFileService::class));
        return $service;
    }
}
