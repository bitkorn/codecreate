<?php

namespace Bitkorn\CodeCreate\Factory\Service\Create\Type;

use Bitkorn\CodeCreate\Service\Create\File\ConfigFileService;
use Bitkorn\CodeCreate\Service\Create\Type\MappingGetterSetterService;
use Bitkorn\Trinket\Service\Filesystem\FolderTool;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class MappingGetterSetterServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new MappingGetterSetterService();
        $service->setLogger($container->get('logger'));
        $service->setFolderTool($container->get(FolderTool::class));
        $service->setAdapter($container->get('dbDefault'));
        return $service;
    }
}
