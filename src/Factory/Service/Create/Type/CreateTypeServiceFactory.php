<?php


namespace Bitkorn\CodeCreate\Factory\Service\Create\Type;


use Bitkorn\CodeCreate\Service\Create\File\ConfigFileService;
use Bitkorn\CodeCreate\Service\Create\Type\CreateTypeService;
use Bitkorn\Trinket\Service\Filesystem\FolderTool;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class CreateTypeServiceFactory implements FactoryInterface
{

    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new CreateTypeService();
        $service->setLogger($container->get('logger'));
        $config = $container->get('config');
        $service->setAutoloadPsr4($config['bitkorn_codecreate']['autoload_psr4']);
        $service->setCodeCreateSupportedTypes($config['bitkorn_codecreate']['supported_types']);
        $service->setConfigFileService($container->get(ConfigFileService::class));
        $service->setFolderTool($container->get(FolderTool::class));
        return $service;
    }
}
