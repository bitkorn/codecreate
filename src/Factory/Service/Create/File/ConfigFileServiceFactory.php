<?php


namespace Bitkorn\CodeCreate\Factory\Service\Create\File;


use Bitkorn\CodeCreate\Service\Create\File\ConfigFileService;
use Bitkorn\Trinket\Service\Filesystem\FolderTool;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ConfigFileServiceFactory implements FactoryInterface
{

    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new ConfigFileService();
        $service->setLogger($container->get('logger'));
        $service->setFolderTool($container->get(FolderTool::class));
        return $service;
    }
}
