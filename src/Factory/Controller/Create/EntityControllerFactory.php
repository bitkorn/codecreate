<?php

namespace Bitkorn\CodeCreate\Factory\Controller\Create;

use Bitkorn\CodeCreate\Controller\Create\EntityController;
use Bitkorn\CodeCreate\Form\Create\BasicForm;
use Bitkorn\CodeCreate\Service\Create\Type\CreateTypeService;
use Bitkorn\CodeCreate\Service\Create\Type\MappingGetterSetterService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class EntityControllerFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$controller = new EntityController();
		$controller->setLogger($container->get('logger'));
        $controller->setMappingGetterSetterService($container->get(MappingGetterSetterService::class));
        $controller->setBasicForm($container->get(BasicForm::class));
		return $controller;
	}
}
