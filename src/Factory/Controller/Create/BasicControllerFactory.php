<?php


namespace Bitkorn\CodeCreate\Factory\Controller\Create;


use Bitkorn\CodeCreate\Controller\Create\BasicController;
use Bitkorn\CodeCreate\Form\Create\BasicForm;
use Bitkorn\CodeCreate\Form\Create\ModuleForm;
use Bitkorn\CodeCreate\Service\Create\Module\CreateModuleService;
use Bitkorn\CodeCreate\Service\Create\Type\CreateTypeService;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class BasicControllerFactory implements FactoryInterface
{

    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new BasicController();
        $controller->setLogger($container->get('logger'));
        $controller->setCreateTypeService($container->get(CreateTypeService::class));
        $controller->setCreateModuleService($container->get(CreateModuleService::class));
        $controller->setBasicForm($container->get(BasicForm::class));
        $controller->setModuleForm($container->get(ModuleForm::class));
//        $config = $container->get('config');
        return $controller;
    }
}
