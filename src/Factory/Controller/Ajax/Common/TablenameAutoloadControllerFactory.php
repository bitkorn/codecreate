<?php

namespace Bitkorn\CodeCreate\Factory\Controller\Ajax\Common;

use Bitkorn\CodeCreate\Controller\Ajax\Common\TablenameAutoloadController;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class TablenameAutoloadControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new TablenameAutoloadController();
        $controller->setLogger($container->get('logger'));
        $controller->setAdapter($container->get('dbDefault'));
        return $controller;
    }
}
