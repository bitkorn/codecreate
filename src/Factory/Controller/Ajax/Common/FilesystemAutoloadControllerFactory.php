<?php


namespace Bitkorn\CodeCreate\Factory\Controller\Ajax\Common;


use Bitkorn\CodeCreate\Controller\Ajax\Common\FilesystemAutoloadController;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class FilesystemAutoloadControllerFactory implements FactoryInterface
{

    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new FilesystemAutoloadController();
        $controller->setLogger($container->get('logger'));
        $config = $container->get('config');
        $controller->setAutoloadPsr4($config['bitkorn_codecreate']['autoload_psr4']);
        return $controller;
    }
}
