<?php

namespace Bitkorn\CodeCreate\Factory\Controller;

use Interop\Container\ContainerInterface;
use Bitkorn\CodeCreate\Controller\TrinketController;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class TrinketControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new TrinketController();
        $controller->setLogger($container->get('logger'));
        $controller->setAdapterDefault($container->get('dbDefault'));
        return $controller;
    }
}
