<?php


namespace Bitkorn\CodeCreate\Factory\Form\Create;


use Bitkorn\CodeCreate\Form\Create\BasicForm;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class BasicFormFactory implements FactoryInterface
{

    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $form = new BasicForm();
        $config = $container->get('config');
        $form->setCodeCreateSupportedTypes($config['bitkorn_codecreate']['supported_types']);
        return $form;
    }
}
