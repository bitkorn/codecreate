<?php


namespace Bitkorn\CodeCreate\Service\Create\Module;


use Bitkorn\CodeCreate\Service\Create\AbstractCreateService;
use Bitkorn\CodeCreate\Service\Create\File\ConfigFileService;
use Bitkorn\CodeCreate\Types\Classes\ModuleClassType;
use Bitkorn\CodeCreate\Types\ModuleConfig\PhpArray;
use Laminas\Code\Generator\DocBlockGenerator;
use Laminas\Code\Generator\FileGenerator;
use Laminas\Config\Config;
use Laminas\Filter\Word\CamelCaseToDash;

class CreateModuleService extends AbstractCreateService
{

    /**
     * @var string
     */
    protected $moduleRootPath = '';

    /**
     * @var string
     */
    protected $draftFolder = '';

    /**
     * https://www.php-fig.org/bylaws/psr-naming-conventions
     * <vendor>/<package>
     *
     * @var string <Vendor>/<ModuleName>
     */
    protected $namespace = '';
    protected $namespaceVendor = '';
    protected $namespaceModulename = '';

    /**
     * @var ConfigFileService
     */
    protected $configFileService;

    /**
     * @param string $moduleRootPath
     */
    public function setModuleRootPath(string $moduleRootPath): void
    {
        $this->moduleRootPath = $moduleRootPath;
    }

    /**
     * @param string $draftFolder
     */
    public function setDraftFolder(string $draftFolder): void
    {
        $this->draftFolder = $draftFolder;
    }

    /**
     * @param string $namespace
     */
    public function setNamespace(string $namespace): void
    {
        $namespaces = explode('\\', $namespace);
        if (count($namespaces) != 2) {
            throw new \RuntimeException('Bad Module Namespace: ' . $namespace);
        }
        $this->namespace = $namespace;
        $this->namespaceVendor = $namespaces[0];
        $this->namespaceModulename = $namespaces[1];
    }

    /**
     * @param ConfigFileService $configFileService
     */
    public function setConfigFileService(ConfigFileService $configFileService): void
    {
        $this->configFileService = $configFileService;
    }

    /**
     * @return bool
     */
    public function createModule(): bool
    {
        $folder = $this->moduleRootPath . '/' . $this->namespaceVendor;
        $this->folderTool->computeFolder($folder);
        $folder .= '/' . $this->namespaceModulename;
        if (file_exists($folder) && is_dir($folder)) {
            throw new \RuntimeException('Module ' . $this->namespace . ' allready exist.');
        }
        $this->folderTool->computeFolder($folder);
        $filter = new CamelCaseToDash();
        $composerArr = json_decode(file_get_contents($this->draftFolder . '/composer.json'), true);
        $composerArr['name'] = strtolower($filter->filter($this->namespaceVendor) . '/' . $filter->filter($this->namespaceModulename));
        $composerArr['time'] = date('Y-m-d');
        $composerArr['autoload']['psr-4'] = [
            $this->namespaceVendor . '\\' . $this->namespaceModulename . '\\' => 'module/' . $this->namespaceVendor . '/' . $this->namespaceModulename . '/src/'
        ];
        $composerArr['autoload-dev']['psr-4'] = [
            $this->namespaceVendor . '\\' . $this->namespaceModulename . '\\' => 'module/' . $this->namespaceVendor . '/' . $this->namespaceModulename . '/test/'
        ];
        file_put_contents($folder . '/composer.json', $this->prettyJson($composerArr));
        $this->folderTool->chmodFile($folder . '/composer.json');

        file_put_contents($folder . '/README.md', '# Laminas Module ' . $composerArr['name']);
        $this->folderTool->chmodFile($folder . '/README.md');

        $this->folderTool->computeFolder($folder . '/config');
        $this->configFileService->setConfigFilename('module.config.php');
        $this->configFileService->computeCodecreateConfig($folder . '/config', $this->namespace);

        $this->folderTool->computeFolder($folder . '/src');
        $moduleType = new ModuleClassType();
        $moduleType->setDirectory($folder . '/src');
        $moduleType->setNamespace($this->namespace);
        $moduleType->setClassname('Module');
        $moduleType->createType($this->folderTool);

        return true;

        /**
         * Ab hier:
         * die root composer.json um das Modul erweitern ...funktioniert sauber
         * die /config/modules.config.php kommt kakke raus: escapte Backslashes & mit ungewollten Array keys
         */
        $composerRootFqfn = realpath($this->moduleRootPath . '/..') . '/composer.json';
        $composerRootArr = json_decode(file_get_contents($composerRootFqfn), true);
        $composerRootArr['autoload']['psr-4'][$this->namespaceVendor . '\\' . $this->namespaceModulename . '\\'] = 'module/' . $this->namespaceVendor . '/' . $this->namespaceModulename . '/src/';
        $composerRootArr['autoload-dev']['psr-4'][$this->namespaceVendor . '\\' . $this->namespaceModulename . '\\'] = 'module/' . $this->namespaceVendor . '/' . $this->namespaceModulename . '/test/';
        file_put_contents($composerRootFqfn, $this->prettyJson($composerRootArr));
//        $this->folderTool->chmodFile($composerRootDir);

        $moduleConfigFqfn = realpath($this->moduleRootPath . '/..') . '/config/modules.config.php';
        $writer = new PhpArray();
        $writer->setUseBracketArraySyntax(true);
        $writer->setUseClassNameScalars(true);

        $file = new FileGenerator();
        $file->setDocBlock(new DocBlockGenerator('List of enabled modules for this application.'));
        $file->setDocBlock(new DocBlockGenerator('This should be an array of module namespaces used in the application.'));
        $file->setFilename($moduleConfigFqfn);
        $moduleConfig = include $moduleConfigFqfn;
        $moduleConfig[] = $this->namespace;
        $config = new Config($moduleConfig, true);
        /**
         * strip first line weil der Writer\PhpArray packt "<?php" mit dazu.
         * FileGenerator guckt nach "<?php" und tut es nicht selber hin wenn er es im Body findet.
         */
        $body = $this->stripFirstLine($writer->processConfig($config->toArray()));
        $file->setBody($body);
        $file->write();

        return true;
    }

    protected function prettyJson(array $array): string
    {
        return str_replace('\/', '/', json_encode($array, JSON_PRETTY_PRINT)); // JSON_UNESCAPED_SLASHES, JSON_PRETTY_PRINT
    }

    /**
     * @param $text
     * @return bool|string
     */
    protected function stripFirstLine($text)
    {
        return substr($text, strpos($text, "\n") + 1);
    }
}
