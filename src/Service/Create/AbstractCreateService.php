<?php


namespace Bitkorn\CodeCreate\Service\Create;


use Bitkorn\Trinket\Service\Filesystem\FolderTool;
use Laminas\Log\Logger;
use Laminas\Stdlib\ArrayUtils;

class AbstractCreateService
{
    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var FolderTool
     */
    protected $folderTool;

    /**
     * @var array
     */
    protected $messages = [];

    /**
     * @param Logger $logger
     */
    public function setLogger(Logger $logger): void
    {
        $this->logger = $logger;
    }

    /**
     * @param FolderTool $folderTool
     */
    public function setFolderTool(FolderTool $folderTool): void
    {
        $this->folderTool = $folderTool;
    }

    /**
     * @param string $message
     */
    public function addMessage(string $message):void
    {
        $this->messages[] = $message;
    }

    /**
     * @param array $messages
     */
    public function addMessages(array $messages): void
    {
        $this->messages[] = ArrayUtils::merge($this->messages, $messages);
    }

    /**
     * @return array
     */
    public function getMessages(): array
    {
        return $this->messages;
    }

}
