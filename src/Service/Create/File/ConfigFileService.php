<?php


namespace Bitkorn\CodeCreate\Service\Create\File;


use Bitkorn\CodeCreate\Service\Create\AbstractCreateService;
use Bitkorn\CodeCreate\Types\ModuleConfig\Elements\ServiceManager\ServiceManagerElement;
use Bitkorn\CodeCreate\Types\ModuleConfig\Elements\ViewHelper\ViewHelperConfigElement;
use Bitkorn\CodeCreate\Types\ModuleConfig\ModuleConfigType;
use Bitkorn\CodeCreate\Types\ModuleConfig\PhpArray;
use Laminas\Code\Generator\DocBlockGenerator;
use Laminas\Code\Generator\FileGenerator;
use Laminas\Config\Config;

/**
 * Class ConfigFileService
 * @package Bitkorn\CodeCreate\Service\Create\File
 * @deprecated macht Kakke
 */
class ConfigFileService extends AbstractCreateService
{
    /**
     * codecreate.config.php
     * module.config.php
     *
     * Use codecreate.config.php
     * because include *.config.php interprets e.g. __DIR__ and puts integers to arrays without keys (['foo','bar']).
     *
     */
    protected $configFilename = 'codecreate.config.php';

    /**
     * @var string
     */
    protected $codeCreateConfigPath = '';

    /**
     * @var string
     */
    public $namespaceModule = '';

    /**
     * @var array Key = clean use (without "as alias"); value = clean use (without "as alias") | "as alias"
     */
    public $uses = [];

    /**
     * @var array All class names in use to prevent double class/alias names in this file.
     */
    protected $useClassNames = [];

    /**
     * @var array All alias names in use to prevent double class/alias names in this file.
     */
    protected $useAliasNames = [];

    /**
     * @var array
     */
    public $configArray = [];

    /**
     * @var ServiceManagerElement[]
     */
    protected $serviceManagerElements = [];

    /**
     * @var ViewHelperConfigElement[]
     */
    protected $viewHelperConfigElements = [];

    /**
     * @param mixed $configFilename
     */
    public function setConfigFilename($configFilename): void
    {
        $this->configFilename = $configFilename;
    }

    /**
     * @param ServiceManagerElement[] $serviceManagerElements
     */
    public function setServiceManagerElements(array $serviceManagerElements): void
    {
        $this->serviceManagerElements = $serviceManagerElements;
    }

    /**
     * @param ViewHelperConfigElement[] $viewHelperConfigElements
     */
    public function setViewHelperConfigElements(array $viewHelperConfigElements): void
    {
        $this->viewHelperConfigElements = $viewHelperConfigElements;
    }

    /**
     * Add additional use statements.
     * It uses allways "as Unique_Alias";
     *
     * @param string $use
     */
    public function addUse(string $use): void
    {
        $useClean = $this->getCleanUse($use);
        if (in_array($useClean, array_keys($this->uses))) {
            return;
        }

        $useParts = explode('\\', $useClean);
        $this->uses[$useClean] = $this->computeUniqeUseAlias($useParts);
        return;
    }

    /**
     * @param array $useParts
     * @param bool $recursive
     * @return string Clean use (without "as alias") | "as alias"
     */
    protected function computeUniqeUseAlias(array $useParts, bool $recursive = true): string
    {
        $usePartPositionReverse = count($useParts) - 1;
        $uniqueUseAlias = $useParts[$usePartPositionReverse - 1] . '_' . $useParts[$usePartPositionReverse];
        if ($recursive && in_array($uniqueUseAlias, $this->uses)) {
            return $this->computeUniqeUseAliasRecursive($uniqueUseAlias, $useParts, $usePartPositionReverse - 2);
        }
        return $uniqueUseAlias;
    }

    protected function computeUniqeUseAliasRecursive(string $uniqueUseAlias, array $useParts, int $usePartPositionReverse): string
    {
        if ($usePartPositionReverse < 0) {
            throw new \RuntimeException('$usePartPositionReverse < 0 in ' . __CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
        }
        $uniqueUseAlias = $useParts[$usePartPositionReverse] . '_' . $uniqueUseAlias;
        if (in_array($uniqueUseAlias, $this->uses)) {
            return $this->computeUniqeUseAliasRecursive($uniqueUseAlias, $useParts, --$usePartPositionReverse);
        }
        return $uniqueUseAlias;
    }

    protected function getCleanUse(string $use): string
    {
        if (($asPos = strripos($use, 'as')) !== false) {
            return substr($use, 0, strripos($use, 'as') - 1);
        }
        return $use;
    }

    protected function getAliasFromUse(string $use): string
    {
        if (($asPos = strripos($use, 'as')) !== false) {
            return substr($use, strripos($use, 'as') + 3);
        }
        return '';
    }

    /**
     * @param string $configDirectory
     * @param string namespaceModule
     * @return bool
     */
    public function computeCodecreateConfig(string $configDirectory, string $namespaceModule): bool
    {
        $this->codeCreateConfigPath = $configDirectory . '/' . $this->configFilename;
        $this->namespaceModule = $namespaceModule;

        if (file_exists($this->codeCreateConfigPath)) {
            /** @var resource $handler */
            $handler = fopen($this->codeCreateConfigPath, 'r');
            while (($line = fgets($handler))) {
                if (strpos($line, 'use ') === 0) {
                    $semikolonStrpos = strpos($line, ';');
                    $addUse = substr($line, 4, $semikolonStrpos - 4);
                    $this->addUse($addUse);
                }
            }
            fclose($handler);

            $this->configArray = include $this->codeCreateConfigPath;
            $this->aliasConfigKeysAndValues($this->configArray);
        }

        return $this->createCodecreateConfig();
    }

    /**
     * @param array $configArray
     * @todo Das geht recursive bestimmt kuerzer!
     */
    protected function aliasConfigKeysAndValues(array &$configArray): void
    {
        foreach ($configArray as $_01key => $_01value) {
            if ($_01key == 'controllers' && is_array($_01value) && !empty($_01value)) {
                foreach ($_01value as $_02key => $_02value) {
                    if ($_02key == 'factories' && is_array($_02value) && !empty($_02value)) {
                        foreach ($_02value as $_03key => $_03value) {
                            if (strpos($_03key, '\\') === false) {
                                continue;
                            }
                            $configArray['controllers']['factories'][$this->uses[$_03key]] = $this->uses[$_03value]; //  . '::class'
                            unset($configArray['controllers']['factories'][$_03key]);
                        }
                    }
                }
            }
            if ($_01key == 'service_manager' && is_array($_01value) && !empty($_01value)) {
                foreach ($_01value as $_02key => $_02value) {
                    if ($_02key == 'factories' && is_array($_02value) && !empty($_02value)) {
                        foreach ($_02value as $_03key => $_03value) {
                            if (strpos($_03key, '\\') === false) {
                                continue;
                            }
                            $configArray['service_manager']['factories'][$this->uses[$_03key]] = $this->uses[$_03value]; //  . '::class'
                            unset($configArray['service_manager']['factories'][$_03key]);
                        }
                    }
                }
            }
            if ($_01key == 'view_helpers' && is_array($_01value) && !empty($_01value)) {
                foreach ($_01value as $_02key => $_02value) {
                    if ($_02key == 'factories' && is_array($_02value) && !empty($_02value)) {
                        foreach ($_02value as $_03key => $_03value) {
                            if (strpos($_03key, '\\') === false) {
                                continue;
                            }
                            $configArray['view_helpers']['factories'][$this->uses[$_03key]] = $this->uses[$_03value]; //  . '::class'
                            unset($configArray['view_helpers']['factories'][$_03key]);
                        }
                    }
                }
            }
        }
    }

    /**
     * Can create an emty (only keys for routes & controllers & services) config file.
     * If $this->configArray not empty and/or $this->namespaceModule & $this->uses not empty then put it into the config file.
     *
     * @return bool
     */
    protected function createCodecreateConfig(): bool
    {
        $writer = new PhpArray();
        $writer->setUseBracketArraySyntax(true);
        $writer->setUseClassNameScalars(true);
        $writer->setUses($this->uses);

        $file = new FileGenerator();
        $file->setNamespace($this->namespaceModule);
        $file->setDocBlock(new DocBlockGenerator('Auto generated file ...do not touch it.'));
        $file->setFilename($this->codeCreateConfigPath);
        $uses = [];
        foreach ($this->uses as $use => $alias) {
            if ($use != $alias) {
                $uses[] = $use . ' as ' . $alias;
            } else {
                $uses[] = $use;
            }
        }
        $file->setUses($uses);

        $config = new Config($this->configArray, true);
        if (empty($this->configArray)) {
            $config->router = [];
            $config->router->routes = [];
            $config->controllers = [];
            $config->controllers->factories = [];
            $config->controllers->invokables = [];
            $config->service_manager = [];
            $config->service_manager->factories = [];
            $config->service_manager->invokables = [];
            $config->view_helpers = [];
            $config->view_helpers->factories = [];
            $config->view_helpers->invokables = [];
            $config->view_helpers->aliases = [];
            $config->view_manager = [];
            $config->view_manager->template_map = [];
            $config->view_manager->template_path_stack = [];
            $config->view_manager->strategies = ['ViewJsonStrategy'];
        }

        if (!isset($config->view_helpers)) {
            $config->view_helpers = [];
            $config->view_helpers->factories = [];
        }
        foreach ($this->viewHelperConfigElements as $viewHelperConfigElement) {
            $config->view_helpers->factories[$viewHelperConfigElement->getConfigKey()] = $this->uses[$viewHelperConfigElement->getConfigValueString()];
        }

        /**
         * strip first line weil der Writer\PhpArray packt "<?php" mit dazu.
         * FileGenerator guckt nach "<?php" und tut es nicht selber hin wenn er es im Body findet.
         */
        $body = $this->stripFirstLine($writer->processConfig($config->toArray()));

        $file->setBody($body);
        $file->write();
        return $this->folderTool->chmodFile($this->codeCreateConfigPath);
    }

    /**
     * @param $text
     * @return bool|string
     */
    protected function stripFirstLine($text)
    {
        return substr($text, strpos($text, "\n") + 1);
    }
}
