<?php

namespace Bitkorn\CodeCreate\Service\Create\Type;

use Bitkorn\CodeCreate\Types\Classes\Entity\EntityType;
use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\ParameterContainer;
use Laminas\Log\Logger;

/**
 * Class MappingGetterSetterService
 * @package Bitkorn\CodeCreate\Service\Create\Type\Functions\EntityClass
 *
 * Create Getter and Setter from database.
 * It takes the table-column names with his data types.
 * Also the data type to generate function parameter and return type hinting.
 *
 * Names for Getter and Setter uses the column names underscore to camel case.
 */
class MappingGetterSetterService extends AbstractCreateTypeService
{
    const TYPENAME = 'Entity';
    protected Adapter $adapter;
    protected string $tableName;

    protected string $querySelect = 'SELECT column_name, udt_name FROM information_schema.columns WHERE table_schema = \'public\' AND table_name = :table_name ORDER BY ordinal_position ASC';

    protected bool $withGetter = true;
    protected bool $withSetter = true;
    protected bool $camelCase = false;
    protected array $columnNames = [];

    public function setAdapter(Adapter $adapter): void
    {
        $this->adapter = $adapter;
    }

    public function setTableName(string $tableName): void
    {
        $this->tableName = $tableName;
    }

    public function setWithGetter(bool $withGetter): void
    {
        $this->withGetter = $withGetter;
    }

    public function setWithSetter(bool $withSetter): void
    {
        $this->withSetter = $withSetter;
    }

    /**
     * Mapping value in camel case? ...or in snake case.
     *
     * @param bool $camelCase
     */
    public function setCamelCase(bool $camelCase): void
    {
        $this->camelCase = $camelCase;
    }

    public function setColumnNames(array $columnNames): void
    {
        $this->columnNames = $columnNames;
    }

    public function createEntity(): bool
    {
        if (strpos($this->classname, self::TYPENAME) === false) {
            $this->classname .= self::TYPENAME;
        }
        $this->createFolders(self::TYPENAME, false);
        $entityType = new EntityType();
        $entityType->setLogger($this->logger);
        $entityType->setWithGetter($this->withGetter);
        $entityType->setWithSetter($this->withSetter);
        $entityType->setCamelCase($this->camelCase);
        $entityType->setDirectory($this->directoryClass);
        $entityType->setNamespace($this->namespaceComplete);
        $entityType->setClassname($this->classname);

        if ($this->tableName) {
            $qParams = new ParameterContainer(['table_name' => $this->tableName]);
            $stmt = $this->adapter->createStatement($this->querySelect, $qParams);
            $result = $stmt->execute();
            if (!$result->valid()) {
                return false;
            }
            $columns = [];
            do {
                $columns[$result->current()['column_name']] = $result->current()['udt_name'];
                $result->next();
            } while ($result->valid());

            $entityType->setColumnNames($columns);
        } else if ($this->columnNames) {
            $entityType->setColumnNames($this->columnNames);
        }
        if (!$entityType->createType($this->folderTool)) {
            return false;
        }
        return true;
    }
}
