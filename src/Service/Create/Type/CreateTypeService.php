<?php

namespace Bitkorn\CodeCreate\Service\Create\Type;

use Bitkorn\CodeCreate\Service\Create\AbstractCreateService;
use Bitkorn\CodeCreate\Service\Create\File\ConfigFileService;
use Bitkorn\CodeCreate\Types\Classes\Command\CommandSymfonyType;
use Bitkorn\CodeCreate\Types\Classes\Controller\ControllerHtmlType;
use Bitkorn\CodeCreate\Types\Classes\Controller\ControllerAjaxType;
use Bitkorn\CodeCreate\Types\Classes\Controller\ControllerRestType;
use Bitkorn\CodeCreate\Types\Classes\Factory\Command\CommandSymfonyFactoryType;
use Bitkorn\CodeCreate\Types\Classes\Factory\Controller\ControllerHtmlFactoryType;
use Bitkorn\CodeCreate\Types\Classes\Factory\Controller\ControllerRestFactoryType;
use Bitkorn\CodeCreate\Types\Classes\Factory\Service\ServiceFactoryType;
use Bitkorn\CodeCreate\Types\Classes\Factory\Form\FormFactoryType;
use Bitkorn\CodeCreate\Types\Classes\Factory\Table\TableFactoryType;
use Bitkorn\CodeCreate\Types\Classes\Factory\View\Helper\ViewHelperFactoryType;
use Bitkorn\CodeCreate\Types\Classes\Form\FormType;
use Bitkorn\CodeCreate\Types\Classes\Service\ServiceType;
use Bitkorn\CodeCreate\Types\Classes\Table\TableType;
use Bitkorn\CodeCreate\Types\Classes\View\Helper\ViewHelperType;
use Bitkorn\CodeCreate\Types\Functions\Controller\Html\ControllerActionHtml;
use Bitkorn\CodeCreate\Types\Functions\Controller\Json\ControllerActionAjax;
use Bitkorn\CodeCreate\Types\Functions\Controller\Rest\ControllerActionRest;
use Bitkorn\CodeCreate\Types\ModuleConfig\Elements\ServiceManager\ServiceManagerElement;
use Bitkorn\CodeCreate\Types\ModuleConfig\Elements\ViewHelper\ViewHelperConfigElement;

class CreateTypeService extends AbstractCreateTypeService
{

    /**
     *
     * @var array key=namespace; value=directorySrc
     */
    protected array $autoloadPsr4 = [];

    /**
     * @var array
     */
    protected array $codeCreateSupportedTypes = [];

    /**
     * @var ConfigFileService
     */
    protected ConfigFileService $configFileService;

    /**
     * @var string One of $this->supportedTypes
     */
    protected string $type;
    protected string $tableName; // e.g. for type table, to have a table name

    /**
     * @var bool
     */
    protected bool $onlyFactory = false;

    public function setAutoloadPsr4(string $autoloadPsr4): void
    {
        $this->autoloadPsr4 = include realpath($autoloadPsr4);
    }

    public function getAutoloadPsr4(): array
    {
        return $this->autoloadPsr4;
    }

    public function setCodeCreateSupportedTypes(array $codeCreateSupportedTypes): void
    {
        $this->codeCreateSupportedTypes = $codeCreateSupportedTypes;
    }

    public function setConfigFileService(ConfigFileService $configFileService): void
    {
        $this->configFileService = $configFileService;
    }

    public function setType(string $type): void
    {
        if (!in_array($type, array_keys($this->codeCreateSupportedTypes))) {
            throw new \RuntimeException('Type ' . $type . ' does not exist.');
        }
        $this->type = $type;
    }

    public function setTableName(string $tableName): void
    {
        $this->tableName = $tableName;
    }

    public function setOnlyFactory(bool $onlyFactory): void
    {
        $this->onlyFactory = $onlyFactory;
    }

    /**
     * @return bool
     */
    public function createType(): bool
    {
        if (empty($this->type)) {
            throw new \RuntimeException('Must set $type before call ' . __FUNCTION__ . '()');
        }
        $success = false;
        switch ($this->type) {
            case 'controller_html':
                $success = $this->createFolders($this->codeCreateSupportedTypes['controller_html']['type_foldername'])
                    && $this->createControllerHtmlType();
                break;
            case 'controller_ajax':
                $success = $this->createFolders($this->codeCreateSupportedTypes['controller_ajax']['type_foldername'])
                    && $this->createControllerAjaxType();
                break;
            case 'controller_rest':
                $success = $this->createFolders($this->codeCreateSupportedTypes['controller_rest']['type_foldername'])
                    && $this->createControllerRestType();
                break;
            case 'command':
                $success = $this->createFolders($this->codeCreateSupportedTypes['command']['type_foldername'])
                    && $this->createCommandType();
                break;
            case 'table':
                $success = $this->createFolders($this->codeCreateSupportedTypes['table']['type_foldername'])
                    && $this->createTableType();
                if (!$success) {
                    return false;
                }
                break;
            case 'form':
                $success = $this->createFolders($this->codeCreateSupportedTypes['form']['type_foldername'])
                    && $this->createFormType();
                if (!$success) {
                    return false;
                }
                break;
            case 'service':
                $success = $this->createFolders($this->codeCreateSupportedTypes['service']['type_foldername'])
                    && $this->createServiceType();
                if (!$success) {
                    return false;
                }
                break;
            case 'view_helper':
                $success = $this->createFolders($this->codeCreateSupportedTypes['view_helper']['type_foldername'])
                    && $this->createHelperType();
                if (!$success) {
                    return false;
                }
                break;
        }

        return $success;
    }

    /**
     * @return bool
     */
    protected function createControllerHtmlType(): bool
    {
        if (strpos($this->classname, 'Controller') === false) {
            $this->classname .= 'Controller';
        }
        if (!$this->onlyFactory) {
            $controllerType = new ControllerHtmlType();
            $controllerType->setDirectory($this->directoryClass);
            $controllerType->setNamespace($this->namespaceComplete);
            $controllerType->setClassname($this->classname);
            $controllerType->addActionFunction(new ControllerActionHtml('test'));
            if (!$controllerType->createType($this->folderTool)) {
                return false;
            }
        }

        $factoryType = new ControllerHtmlFactoryType();
        $factoryType->setDirectory($this->directoryFactory);
        $factoryType->setNamespace($this->namespaceCompleteFactory);
        $factoryType->setClassnameCompleteMake($this->namespaceComplete . '\\' . $this->classname);
        $factoryType->setClassname($this->classname . 'Factory');
        if (!$factoryType->createType($this->folderTool)) {
            return false;
        }
        return true;

//        $serviceManagerElement = new ServiceManagerElement();
//        $serviceManagerElement->setConfigKey($this->namespaceComplete . '\\' . $this->classname);
//        $serviceManagerElement->setConfigValueString($this->namespaceCompleteFactory . '\\' . $this->classname . 'Factory');
//
//        $this->configFileService->addUse($this->namespaceComplete . '\\' . $this->classname);
//        $this->configFileService->addUse($this->namespaceCompleteFactory . '\\' . $this->classname . 'Factory');
//        $this->configFileService->setServiceManagerElements([$serviceManagerElement]);
//        return $this->configFileService->computeCodecreateConfig($this->directoryModule . '/config', $this->namespaceModule);
    }

    /**
     * @return bool
     */
    protected function createControllerAjaxType(): bool
    {
        if (strpos($this->classname, 'Controller') === false) {
            $this->classname .= 'Controller';
        }
        if (!$this->onlyFactory) {
            $controllerType = new ControllerAjaxType();
            $controllerType->setDirectory($this->directoryClass);
            $controllerType->setNamespace($this->namespaceComplete);
            $controllerType->setClassname($this->classname);
            $controllerType->addActionFunction(new ControllerActionAjax('test'));
            if (!$controllerType->createType($this->folderTool)) {
                return false;
            }
        }

        $factoryType = new ControllerHtmlFactoryType();
        $factoryType->setDirectory($this->directoryFactory);
        $factoryType->setNamespace($this->namespaceCompleteFactory);
        $factoryType->setClassnameCompleteMake($this->namespaceComplete . '\\' . $this->classname);
        $factoryType->setClassname($this->classname . 'Factory');
        if (!$factoryType->createType($this->folderTool)) {
            return false;
        }
        return true;

//        $serviceManagerElement = new ServiceManagerElement();
//        $serviceManagerElement->setConfigKey($this->namespaceComplete . '\\' . $this->classname);
//        $serviceManagerElement->setConfigValueString($this->namespaceCompleteFactory . '\\' . $this->classname . 'Factory');
//
//        $this->configFileService->addUse($this->namespaceComplete . '\\' . $this->classname);
//        $this->configFileService->addUse($this->namespaceCompleteFactory . '\\' . $this->classname . 'Factory');
//        $this->configFileService->setServiceManagerElements([$serviceManagerElement]);
//        return true;
//        return $this->configFileService->computeCodecreateConfig($this->directoryModule . '/config', $this->namespaceModule);
    }

    /**
     * @return bool
     */
    protected function createControllerRestType(): bool
    {
        if (strpos($this->classname, 'Controller') === false) {
            $this->classname .= 'Controller';
        }
        if (!$this->onlyFactory) {
            $controllerType = new ControllerRestType();
            $controllerType->setDirectory($this->directoryClass);
            $controllerType->setNamespace($this->namespaceComplete);
            $controllerType->setClassname($this->classname);
//        $actionGet = new ControllerActionRest();
//        $actionGet->setRestMethod('PUT');
//        $controllerType->addActionFunction($actionGet);
            if (!$controllerType->createType($this->folderTool)) {
                return false;
            }
        }

        $factoryType = new ControllerRestFactoryType();
        $factoryType->setDirectory($this->directoryFactory);
        $factoryType->setNamespace($this->namespaceCompleteFactory);
        $factoryType->setClassnameCompleteMake($this->namespaceComplete . '\\' . $this->classname);
        $factoryType->setClassname($this->classname . 'Factory');
        if (!$factoryType->createType($this->folderTool)) {
            return false;
        }
        return true;

//        $serviceManagerElement = new ServiceManagerElement();
//        $serviceManagerElement->setConfigKey($this->namespaceComplete . '\\' . $this->classname);
//        $serviceManagerElement->setConfigValueString($this->namespaceCompleteFactory . '\\' . $this->classname . 'Factory');
//
//        $this->configFileService->setServiceManagerElements([$serviceManagerElement]);
//
//        $this->configFileService->addUse($this->namespaceComplete . '\\' . $this->classname);
//        $this->configFileService->addUse($this->namespaceCompleteFactory . '\\' . $this->classname . 'Factory');
//        return true;
//        return $this->configFileService->computeCodecreateConfig($this->directoryModule . '/config', $this->namespaceModule);
    }

    protected function createCommandType(): bool
    {
        if (strpos($this->classname, 'Command') === false) {
            $this->classname .= 'Command';
        }
        if (!$this->onlyFactory) {
            $commandType = new CommandSymfonyType();
            $commandType->setDirectory($this->directoryClass);
            $commandType->setNamespace($this->namespaceComplete);
            $commandType->setClassname($this->classname);
            if (!$commandType->createType($this->folderTool)) {
                return false;
            }
        }

        $factoryType = new CommandSymfonyFactoryType();
        $factoryType->setDirectory($this->directoryFactory);
        $factoryType->setNamespace($this->namespaceCompleteFactory);
        $factoryType->setClassnameCompleteMake($this->namespaceComplete . '\\' . $this->classname);
        $factoryType->setClassname($this->classname . 'Factory');
        if (!$factoryType->createType($this->folderTool)) {
            return false;
        }
        return true;
    }

    /**
     * @return bool
     */
    protected function createTableType(): bool
    {
        if (strpos($this->classname, 'Table') === false && strpos($this->classname, 'Tablex') === false) {
            $this->classname .= 'Table';
        }
        if (!$this->onlyFactory) {
            $type = new TableType();
            $type->setDirectory($this->directoryClass);
            $type->setNamespace($this->namespaceComplete);
            $type->setClassname($this->classname);
            $type->setTableName($this->tableName ?? '');
            if (!$type->createType($this->folderTool)) {
                return false;
            }
        }

        $factoryType = new TableFactoryType();
        $factoryType->setDirectory($this->directoryFactory);
        $factoryType->setNamespace($this->namespaceCompleteFactory);
        $factoryType->setClassnameCompleteMake($this->namespaceComplete . '\\' . $this->classname);
        $factoryType->setClassname($this->classname . 'Factory');
        if (!$factoryType->createType($this->folderTool)) {
            return false;
        }
        return true;

//        $serviceManagerElement = new ServiceManagerElement();
//        $serviceManagerElement->setConfigKey($this->namespaceComplete . '\\' . $this->classname);
//        $serviceManagerElement->setConfigValueString($this->namespaceCompleteFactory . '\\' . $this->classname . 'Factory');
//
//        $this->configFileService->setServiceManagerElements([$serviceManagerElement]);
//
//        $this->configFileService->addUse($this->namespaceComplete . '\\' . $this->classname);
//        $this->configFileService->addUse($this->namespaceCompleteFactory . '\\' . $this->classname . 'Factory');
//        return true;
//        return $this->configFileService->computeCodecreateConfig($this->directoryModule . '/config', $this->namespaceModule);
    }

    /**
     * @return bool
     */
    protected function createFormType(): bool
    {
        if (strpos($this->classname, 'Form') === false) {
            $this->classname .= 'Form';
        }
        if (!$this->onlyFactory) {
            $type = new FormType();
            $type->setDirectory($this->directoryClass);
            $type->setNamespace($this->namespaceComplete);
            $type->setClassname($this->classname);
            if (!$type->createType($this->folderTool)) {
                return false;
            }
        }

        $factoryType = new FormFactoryType();
        $factoryType->setDirectory($this->directoryFactory);
        $factoryType->setNamespace($this->namespaceCompleteFactory);
        $factoryType->setClassnameCompleteMake($this->namespaceComplete . '\\' . $this->classname);
        $factoryType->setClassname($this->classname . 'Factory');
        if (!$factoryType->createType($this->folderTool)) {
            return false;
        }
        return true;

//        $serviceManagerElement = new ServiceManagerElement();
//        $serviceManagerElement->setConfigKey($this->namespaceComplete . '\\' . $this->classname);
//        $serviceManagerElement->setConfigValueString($this->namespaceCompleteFactory . '\\' . $this->classname . 'Factory');
//
//        $this->configFileService->setServiceManagerElements([$serviceManagerElement]);
//
//        $this->configFileService->addUse($this->namespaceComplete . '\\' . $this->classname);
//        $this->configFileService->addUse($this->namespaceCompleteFactory . '\\' . $this->classname . 'Factory');
//        return true;
//        return $this->configFileService->computeCodecreateConfig($this->directoryModule . '/config', $this->namespaceModule);
    }

    /**
     * @return bool
     */
    protected function createServiceType(): bool
    {
        if (strpos($this->classname, 'Service') === false) {
            $this->classname .= 'Service';
        }
        if (!$this->onlyFactory) {
            $type = new ServiceType();
            $type->setDirectory($this->directoryClass);
            $type->setNamespace($this->namespaceComplete);
            $type->setClassname($this->classname);
            if (!$type->createType($this->folderTool)) {
                return false;
            }
        }

        $factoryType = new ServiceFactoryType();
        $factoryType->setDirectory($this->directoryFactory);
        $factoryType->setNamespace($this->namespaceCompleteFactory);
        $factoryType->setClassnameCompleteMake($this->namespaceComplete . '\\' . $this->classname);
        $factoryType->setClassname($this->classname . 'Factory');
        if (!$factoryType->createType($this->folderTool)) {
            return false;
        }
        return true;

//        $serviceManagerElement = new ServiceManagerElement();
//        $serviceManagerElement->setConfigKey($this->namespaceComplete . '\\' . $this->classname);
//        $serviceManagerElement->setConfigValueString($this->namespaceCompleteFactory . '\\' . $this->classname . 'Factory');
//
//        $this->configFileService->setServiceManagerElements([$serviceManagerElement]);
//
//        $this->configFileService->addUse($this->namespaceComplete . '\\' . $this->classname);
//        $this->configFileService->addUse($this->namespaceCompleteFactory . '\\' . $this->classname . 'Factory');
//        return true;
//        return $this->configFileService->computeCodecreateConfig($this->directoryModule . '/config', $this->namespaceModule);
    }

    /**
     * @return bool
     */
    protected function createHelperType(): bool
    {
        if (false && strpos($this->classname, 'Helper') === false) {
            $this->classname .= 'Helper';
        }
        if (!$this->onlyFactory) {
            $type = new ViewHelperType();
            $type->setDirectory($this->directoryClass);
            $type->setNamespace($this->namespaceComplete);
            $type->setClassname($this->classname);
            if (!$type->createType($this->folderTool)) {
                return false;
            }
        }

        $factoryType = new ViewHelperFactoryType();
        $factoryType->setDirectory($this->directoryFactory);
        $factoryType->setNamespace($this->namespaceCompleteFactory);
        $factoryType->setClassnameCompleteMake($this->namespaceComplete . '\\' . $this->classname);
        $factoryType->setClassname($this->classname . 'Factory');
        if (!$factoryType->createType($this->folderTool)) {
            return false;
        }
        return true;

//        $viewHelperConfigElement = new ViewHelperConfigElement();
//        $viewHelperConfigElement->setConfigKey(lcfirst($this->classname));
//        $viewHelperConfigElement->setConfigValueString($this->namespaceCompleteFactory . '\\' . $this->classname . 'Factory');
//
//        $this->configFileService->setViewHelperConfigElements([$viewHelperConfigElement]);
//
//        $this->configFileService->addUse($this->namespaceComplete . '\\' . $this->classname);
//        $this->configFileService->addUse($this->namespaceCompleteFactory . '\\' . $this->classname . 'Factory');
//        return true;
//        return $this->configFileService->computeCodecreateConfig($this->directoryModule . '/config', $this->namespaceModule);
    }
}
