<?php

namespace Bitkorn\CodeCreate\Service\Create\Type;

use Bitkorn\CodeCreate\Service\Create\AbstractCreateService;

class AbstractCreateTypeService extends AbstractCreateService
{

    /**
     * https://www.php-fig.org/bylaws/psr-naming-conventions
     * <vendor>/<package>
     *
     * @var
     */
    protected string $namespaceModule;
    protected array $namespaceModuleParts = [];
    protected string $namespaceClass;
    protected array $namespacePartsClass = [];
    protected string $namespaceComplete;
    protected string $namespaceCompleteFactory;

    /**
     * @var string Directory suitable for namespace
     */
    protected string $directorySrc;

    /**
     * @var string Directory suitable for namespace
     */
    protected string $directoryModule;

    /**
     * @var string Directory suitable for namespace
     */
    protected string $directoryClass;

    /**
     * @var string Directory suitable for namespace
     */
    protected string $directoryFactory;

    protected string $classname;

    /**
     * https://www.php-fig.org/bylaws/psr-naming-conventions
     * <vendor>/<package>
     *
     * @param string $namespaceModule
     */
    public function setNamespaceModule(string $namespaceModule): void
    {
        $this->folderTool->removePrependetNamespaceSeparators($namespaceModule);
        $this->folderTool->removeAppendetNamespaceSeparators($namespaceModule);
        $this->namespaceModule = $namespaceModule;
        $this->namespaceModuleParts = explode('\\', $namespaceModule);
        $this->namespaceComplete = $namespaceModule; // latter more appended
        $this->namespaceCompleteFactory = $namespaceModule . '\\Factory'; // latter more appended
    }

    /**
     * @param string $namespaceClass
     */
    public function setNamespaceClass(string $namespaceClass): void
    {
        $this->folderTool->removePrependetNamespaceSeparators($namespaceClass);
        $this->folderTool->removeAppendetNamespaceSeparators($namespaceClass);
        $this->namespaceClass = $namespaceClass;
        $this->namespacePartsClass = explode('\\', $namespaceClass);
    }

    /**
     * @param string $directorySrc
     */
    public function setDirectorySrc(string $directorySrc): void
    {
        $this->folderTool->removeAppendetDirectorySeparators($directorySrc);
        $this->directorySrc = $this->directoryClass = $directorySrc;
        $this->directoryFactory = $this->directoryClass . '/Factory';
        $this->directoryModule = substr($directorySrc, 0, strlen($directorySrc) - 4);
    }

    /**
     * @param string $classname
     */
    public function setClassname(string $classname): void
    {
        $this->classname = $classname;
    }

    /**
     * Creates folders and complete directories & namespaces
     * @param string $typeFolderName
     * @param bool $withFactory
     * @return bool
     */
    protected function createFolders(string $typeFolderName, bool $withFactory = true): bool
    {
        if (!$this->folderTool->computeFolder($this->directoryClass) || ($withFactory && !$this->folderTool->computeFolder($this->directoryFactory))) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__ . ' Error while create folders');
        }
        if (!in_array($typeFolderName, $this->namespacePartsClass) && !in_array($typeFolderName . 'x', $this->namespacePartsClass)) {
            $this->directoryClass .= DIRECTORY_SEPARATOR . $typeFolderName;
            $this->namespaceComplete .= '\\' . $typeFolderName;
            if ($withFactory) {
                $this->directoryFactory .= DIRECTORY_SEPARATOR . $typeFolderName;
                $this->namespaceCompleteFactory .= '\\' . $typeFolderName;
            }
            if (!$this->folderTool->computeFolder($this->directoryClass) || ($withFactory && !$this->folderTool->computeFolder($this->directoryFactory))) {
                throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__ . ' Error while create folders');
            }
        }
        foreach ($this->namespacePartsClass as $namespacePartClass) {
            if (empty($namespacePartClass)) {
                continue;
            }
            $this->directoryClass .= DIRECTORY_SEPARATOR . $namespacePartClass;
            $this->namespaceComplete .= '\\' . $namespacePartClass;
            if ($withFactory) {
                $this->directoryFactory .= DIRECTORY_SEPARATOR . $namespacePartClass;
                $this->namespaceCompleteFactory .= '\\' . $namespacePartClass;
            }
            if (!$this->folderTool->computeFolder($this->directoryClass) || ($withFactory && !$this->folderTool->computeFolder($this->directoryFactory))) {
                throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__ . ' Error while create folders');
            }
        }
        return true;
    }

}
