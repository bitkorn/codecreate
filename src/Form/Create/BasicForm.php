<?php


namespace Bitkorn\CodeCreate\Form\Create;


use Laminas\Filter\HtmlEntities;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\Form\Element\Checkbox;
use Laminas\Form\Element\MultiCheckbox;
use Laminas\Form\Element\Select;
use Laminas\Form\Element\Submit;
use Laminas\Form\Element\Text;
use Laminas\Form\Element\Textarea;
use Laminas\Form\Form;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\InArray;
use Laminas\Validator\StringLength;

class BasicForm extends Form implements InputFilterProviderInterface
{
    protected array $codeCreateSupportedTypes = [];

    protected bool $entityType = false;

    public function setCodeCreateSupportedTypes(array $codeCreateSupportedTypes): void
    {
        foreach ($codeCreateSupportedTypes as $key => $codeCreateSupportedType) {
            $this->codeCreateSupportedTypes[$key] = $codeCreateSupportedType['desc'];
        }
    }

    public function setEntityType(bool $entityType): void
    {
        $this->entityType = $entityType;
    }

    /**
     *
     */
    public function init()
    {
        $this->setAttribute('class', 'w3-container');
        $this->setAttribute('id', 'form_create_basic');

        $this->add([
            'name' => 'directory_src',
            'type' => Text::class,
            'options' => [
                'label' => 'Directory src',
                'label_attributes' => [
                    'class' => 'label-input'
                ]
            ],
            'attributes' => [
                'id' => 'form_directory_src',
                'class' => 'w3-input w3-border input-readonly',
                'readonly' => 'readonly',
                'placeholder' => 'auto filled'
            ]
        ]);

        $this->add([
            'name' => 'namespace_module',
            'type' => Text::class,
            'options' => [
                'label' => 'Module Namespace (<vendor>\<modulename>)',
                'label_attributes' => [
                    'class' => 'label-input'
                ]
            ],
            'attributes' => [
                'id' => 'form_namespace_module',
                'class' => 'w3-input w3-border',
                'placeholder' => 'Module Namespace'
            ]
        ]);

        $this->add([
            'name' => 'namespace_class',
            'type' => Text::class,
            'options' => [
                'label' => 'Klassen Namespace',
                'label_attributes' => [
                    'class' => 'label-input'
                ]
            ],
            'attributes' => [
                'id' => 'form_namespace_class',
                'class' => 'w3-input w3-border',
                'placeholder' => 'Klassen Namespace'
            ]
        ]);

        $this->add([
            'name' => 'classname',
            'type' => Text::class,
            'options' => [
                'label' => 'Klassenname',
                'label_attributes' => [
                    'class' => 'label-input'
                ]
            ],
            'attributes' => [
                'id' => 'form_classname',
                'class' => 'w3-input w3-border',
                'placeholder' => 'Klassenname'
            ]
        ]);

        if (!$this->entityType) {
            $this->add([
                'name' => 'codecreate_type',
                'type' => Select::class,
                'options' => [
                    'label' => 'Type',
                    'value_options' => $this->codeCreateSupportedTypes
                ],
                'attributes' => [
                    'class' => 'w3-select w3-border',
                    'id' => 'codecreate_type',
                ]
            ]);

            $this->add([
                'name' => 'table_name',
                'type' => Text::class,
                'options' => [
                    'label' => 'Tabellen Name',
                    'label_attributes' => [
                        'class' => 'label-input w3-hide',
                        'id' => 'table_name_label'
                    ]
                ],
                'attributes' => [
                    'id' => 'table_name',
                    'class' => 'w3-input w3-border w3-hide',
                    'placeholder' => 'foo_bar'
                ]
            ]);

            $this->add([
                'name' => 'only_factory',
                'type' => Checkbox::class,
                'options' => [
                    'label' => 'nur die Factory ',
                    'label_attributes' => [
                        'position' => 'append',
                        'class' => 'label-input',
                        'style' => 'display: block'
                    ],
                    'use_hidden_element' => false,
                    'checked_value' => '1',
                    'unchecked_value' => '0',
                ],
                'attributes' => [
                    'class' => 'w3-check'
                ]
            ]);
        } else {
            $this->add([
                'name' => 'table_name',
                'type' => Text::class,
                'options' => [
                    'label' => 'Tabellen Name',
                    'label_attributes' => [
                        'class' => 'label-input'
                    ]
                ],
                'attributes' => [
                    'id' => 'table_name',
                    'class' => 'w3-input w3-border',
                    'placeholder' => 'foo_bar'
                ]
            ]);

            $this->add([
                'name' => 'column_names',
                'type' => Textarea::class,
                'options' => [
                    'label' => 'Tabellen Spalten Namen als CSV - mit Datentyp',
                    'label_attributes' => [
                        'class' => 'label-input'
                    ]
                ],
                'attributes' => [
                    'id' => 'table_name',
                    'class' => 'w3-input w3-border',
                    'placeholder' => 'fieldname fieldtype, fieldname2 fieldtype2'
                ]
            ]);

            $this->add([
                'name' => 'with_getter',
                'type' => Checkbox::class,
                'options' => [
                    'label' => 'mit Gettern ',
                    'label_attributes' => [
                        'position' => 'append',
                        'class' => 'label-input',
                        'style' => 'display: block'
                    ],
                    'use_hidden_element' => false,
                    'checked_value' => '1',
                    'unchecked_value' => '0',
                ],
                'attributes' => [
                    'class' => 'w3-check'
                ]
            ]);

            $this->add([
                'name' => 'with_setter',
                'type' => Checkbox::class,
                'options' => [
                    'label' => 'mit Settern ',
                    'label_attributes' => [
                        'position' => 'append',
                        'class' => 'label-input',
                        'style' => 'display: block'
                    ],
                    'use_hidden_element' => false,
                    'checked_value' => '1',
                    'unchecked_value' => '0',
                ],
                'attributes' => [
                    'class' => 'w3-check'
                ]
            ]);

            $this->add([
                'name' => 'camel_case',
                'type' => Checkbox::class,
                'options' => [
                    'label' => 'Camel Case ',
                    'title' => 'Mapping Value in camel case?',
                    'label_attributes' => [
                        'class' => 'label-input',
                        'style' => 'display: block'
                    ],
                    'use_hidden_element' => false,
                    'checked_value' => '1',
                    'unchecked_value' => '0',
                ],
                'attributes' => [
                    'class' => 'w3-check'
                ]
            ]);
        }

        $this->add([
            'name' => 'submit',
            'type' => Submit::class,
            'attributes' => [
                'value' => 'create',
                'class' => 'w3-button w3-grey w3-section'
            ]
        ]);
    }

    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $filter = [];

        $filter['directory_src'] = [
            'required' => true,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class]
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 2,
                        'max' => 1000,
                    ]
                ]
            ]
        ];

        $filter['namespace_module'] = [
            'required' => true,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class]
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 2,
                        'max' => 1000,
                    ]
                ]
            ]
        ];

        $filter['namespace_class'] = [
            'required' => false,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class]
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 2,
                        'max' => 1000,
                    ]
                ]
            ]
        ];

        $filter['classname'] = [
            'required' => true,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class]
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 2,
                        'max' => 100,
                    ]
                ]
            ]
        ];

        if (!$this->entityType) {
            $filter['codecreate_type'] = [
                'required' => true,
                'filters' => [],
                'validators' => [
                    [
                        'name' => InArray::class,
                        'options' => [
                            'haystack' => array_keys($this->codeCreateSupportedTypes)
                        ]
                    ]
                ]
            ];

            $filter['only_factory'] = [
                'required' => false,
                'filters' => [],
                'validators' => [
                    [
                        'name' => InArray::class,
                        'options' => [
                            'haystack' => [0, 1]
                        ]
                    ]
                ]
            ];
        } else {

            $filter['table_name'] = [
                'required' => false,
                'filters' => [
                    ['name' => StripTags::class],
                    ['name' => StringTrim::class],
                    ['name' => HtmlEntities::class]
                ],
                'validators' => [
                    [
                        'name' => StringLength::class,
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 2,
                            'max' => 100,
                        ]
                    ]
                ]
            ];
            $filter['column_names'] = [
                'required' => false,
                'filters' => [
                    ['name' => StripTags::class],
                    ['name' => StringTrim::class],
                    ['name' => HtmlEntities::class]
                ],
                'validators' => [
                    [
                        'name' => StringLength::class,
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min' => 2,
                            'max' => 60000,
                        ]
                    ]
                ]
            ];
            $filter['with_getter'] = [
                'required' => false,
                'filters' => [],
                'validators' => [
                    [
                        'name' => InArray::class,
                        'options' => [
                            'haystack' => [0, 1]
                        ]
                    ]
                ]
            ];
            $filter['with_setter'] = [
                'required' => false,
                'filters' => [],
                'validators' => [
                    [
                        'name' => InArray::class,
                        'options' => [
                            'haystack' => [0, 1]
                        ]
                    ]
                ]
            ];
            $filter['camel_case'] = [
                'required' => false,
                'filters' => [],
                'validators' => [
                    [
                        'name' => InArray::class,
                        'options' => [
                            'haystack' => [0, 1]
                        ]
                    ]
                ]
            ];
        }

        return $filter;
    }

}
