<?php


namespace Bitkorn\CodeCreate\Form\Create;


use Laminas\Filter\HtmlEntities;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\Form\Element\Select;
use Laminas\Form\Element\Submit;
use Laminas\Form\Element\Text;
use Laminas\Form\Form;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\InArray;
use Laminas\Validator\StringLength;

class ModuleForm extends Form implements InputFilterProviderInterface
{

    /**
     *
     */
    public function init()
    {
        $this->setAttribute('class', 'w3-container');


        $this->add([
            'name' => 'module_namespace',
            'type' => Text::class,
            'options' => [
                'label' => 'Module Namespace (<vendor>\<modulename>)',
                'label_attributes' => [
                    'class' => 'label-input'
                ]
            ],
            'attributes' => [
                'id' => 'module_namespace',
                'class' => 'w3-input w3-border',
                'placeholder' => 'Module Namespace'
            ]
        ]);

        $this->add([
            'name' => 'submit',
            'type' => Submit::class,
            'attributes' => [
                'value' => 'create',
                'class' => 'w3-button w3-grey w3-section'
            ]
        ]);
    }

    /**
     * Should return an array specification compatible with
     * {@link Laminas\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $filter = [];

        $filter['module_namespace'] = [
            'required' => true,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
                ['name' => HtmlEntities::class]
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 2,
                        'max' => 1000,
                    ]
                ]
            ]
        ];

        return $filter;
    }

}
