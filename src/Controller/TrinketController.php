<?php

namespace Bitkorn\CodeCreate\Controller;

use Bitkorn\Trinket\Controller\AbstractHtmlController;
use Bitkorn\Trinket\Tools\Database\EntityMapping;
use Bitkorn\Trinket\View\Model\JsonModel;
use Laminas\Db\Adapter\Adapter;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\PhpEnvironment\Response;
use Laminas\View\Model\ViewModel;

class TrinketController extends AbstractHtmlController
{
    /**
     * @var Adapter
     */
    protected $adapterDefault;

    /**
     * @param Adapter $adapterDefault
     */
    public function setAdapterDefault(Adapter $adapterDefault): void
    {
        $this->adapterDefault = $adapterDefault;
    }

    /**
     * @return ViewModel
     */
    public function dbTableMappingAction()
    {
        $jsonModel = new JsonModel();

        $mappingString = EntityMapping::computeEntityMappingAdvanced($this->adapterDefault, $this->params()->fromQuery('table'));
        $this->logger->info($mappingString);
        return $jsonModel;
    }

    /**
     * @return ViewModel
     */
    public function dbTableMappingPostgresqlAction()
    {
        $jsonModel = new JsonModel();
        $mappingString = EntityMapping::computeEntityMappingPostgresql($this->adapterDefault, $this->params()->fromQuery('table'));
        $this->logger->info($mappingString);
        return $jsonModel;
    }
}
