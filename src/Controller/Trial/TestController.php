<?php

namespace Bitkorn\CodeCreate\Controller\Trial;

use Bitkorn\CodeCreate\Controller\AbstractHttpController;
use Nette\PhpGenerator\PhpFile;
use Laminas\Code\Generator\BodyGenerator;
use Laminas\Code\Generator\DocBlockGenerator;
use Laminas\Code\Generator\ParameterGenerator;
use Laminas\Code\Generator\FileGenerator;
use Laminas\Config\Config;
use Laminas\Config\Writer\PhpArray;
use Laminas\Router\Http\Literal;
use Laminas\View\Model\ViewModel;

class TestController extends AbstractHttpController
{

    /**
     * @var array
     */
    protected $config;

    /**
     * @return ViewModel
     */
    public function routeToConfigZendAction(): ViewModel
    {
        $viewModel = new ViewModel();

        /**
         * Habe ich den Module Namespace, kann ich den Pfad der Module.php nehmen
         */
        $namespace = 'Bitkorn\CodeCreate'; // kommt dann aus dem Formular
        try {
            $moduleReflect = new \ReflectionClass('\\' . $namespace . '\Module');
        } catch (\ReflectionException $exception) {
            $this->logger->err($exception->getMessage());
            $this->logger->err($exception->getFile());
            $this->logger->err($exception->getLine());
        }

        /**
         * routes muessen eigene config.php haben weil namespace & use geloescht werden.
         * @todo Oder fromFile generieren und gucken ob namespace und use erhalten bleiben
         */
        $path = str_replace('src/Module.php', '', $moduleReflect->getFileName() . 'config/codecreate.config.php');
        $codecreateConfig = include $path;
        $viewModel->setVariable('path', $path);

        $config = new Config($codecreateConfig, true);
        if(!isset($config->router)) {
            $config->router = [];
        }
        if(!isset($config->router->routes)) {
            $config->router->routes = [];
        }
        $config->router->routes->test1_route = [];
        $config->router->routes->test1_route->type = Literal::class;
        $config->router->routes->test1_route->options = [];
        $config->router->routes->test1_route->options->route = '/dynamic-set';
        $config->router->routes->test1_route->options->defaults = [];
        $config->router->routes->test1_route->options->defaults->controller = self::class;
        $config->router->routes->test1_route->options->defaults->action = 'doIt';

//        $config->router->routes->test2_route = [];
//        $config->router->routes->test2_route->type = Literal::class;
//        $config->router->routes->test2_route->options = [];
//        $config->router->routes->test2_route->options->route = '/dynamic-set';
//        $config->router->routes->test2_route->options->defaults = [];
//        $config->router->routes->test2_route->options->defaults->controller = self::class;
//        $config->router->routes->test2_route->options->defaults->action = 'asdf';

        $writer = new PhpArray();
        $writer->setUseBracketArraySyntax(true);
        $writer->setUseClassNameScalars(true);

        $file = new FileGenerator();
        $file->setNamespace($namespace);
        $file->setUses(['Laminas\Router\Http\Literal', 'Laminas\Router\Http\Segment']);
        /**
         * new DocBlockGenerator('Auto generated file ...do not touch it.')
         */
        $file->setDocBlock(new DocBlockGenerator('Auto generated file ...do not touch it.'));
        $file->setFilename($path);

        /**
         * strip first line weil der Writer\PhpArray packt "<?php" mit dazu.
         * FileGenerator guckt nach "<?php" und tut es nicht selber hin wenn er es im Body findet.
         */
        $body = $this->stripFirstLine($writer->toString($config));
        $file->setBody($body);
        $file->write();

//        file_put_contents($path, $writer->toString($config));
//        file_put_contents($path, $file->generate());

        $viewModel->setVariable('code', $file->getBody());
        return $viewModel;
    }

    /**
     * @todo brauch man das noch?
     * @return ViewModel
     */
    public function routeToConfigNetteAction(): ViewModel
    {
        $viewModel = new ViewModel();

        return $viewModel;
    }

    /**
     * @param $text
     * @return bool|string
     */
    protected function stripFirstLine($text)
    {
        return substr($text, strpos($text, "\n") + 1);
    }

    /**
     * @param array $config
     */
    public function setConfig(array $config): void
    {
        $this->config = $config;
    }

}
