<?php

namespace Bitkorn\CodeCreate\Controller\Trial\Netteway;

use Nette\PhpGenerator\PhpNamespace;
use Nette\PhpGenerator\PsrPrinter;
use Laminas\Http\PhpEnvironment\Response;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;

class NettewayController extends AbstractActionController
{

    /**
     * # GET /bitkorn-codecreate-netteway
     *
     * @return ViewModel
     *
     */
    public function doitAction(): ViewModel
    {
        $viewModel = new ViewModel();

        $viewModel->setVariable('code', $this->generateCode());
        return $viewModel;
    }

    /**
     * https://github.com/nette/php-generator
     *
     * @return string
     */
    protected function generateCode(): string
    {
        $namespace = new PhpNamespace('Foo');
        $namespace->addUse('Bar\AliasedClass');
        $namespace->addUse('Bar\X');

        $class = $namespace->addClass('Demo');
        $class->addImplement('Foo\A')// it will resolve to A
        ->addTrait('Bar\AliasedClass'); // it will resolve to AliasedClass

        $method = $class->addMethod('method');
        $method->addComment('@return ' . $namespace->unresolveName('Foo\D')); // in comments resolve manually
        $method->addParameter('arg')
            ->setType('Bar\OtherClass'); // it will resolve to \Bar\OtherClass
        $method->setReturnType('\Foo\D'); // it will resolve to D, because it is the same namespace
        $method->setBody('return new D();');

//        return $namespace->__toString();
        // or use PsrPrinter for output compatible with PSR-2 / PSR-12
        return (new PsrPrinter())->printNamespace($namespace);
    }
}
