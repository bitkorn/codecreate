<?php

namespace Bitkorn\CodeCreate\Controller\Trial\Zendway;

use Laminas\Code\Generator\ClassGenerator;
use Laminas\Code\Generator\DocBlock\Tag\ParamTag;
use Laminas\Code\Generator\DocBlock\Tag\ReturnTag;
use Laminas\Code\Generator\DocBlockGenerator;
use Laminas\Code\Generator\MethodGenerator;
use Laminas\Code\Generator\ParameterGenerator;
use Laminas\Code\Generator\PropertyGenerator;
use Laminas\Http\PhpEnvironment\Response;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;

class ZendwayController extends AbstractActionController
{

    /**
     * # GET /bitkorn-codecreate-zendway
     * Generate and show code like in https://zendframework.github.io/zend-code/generator/examples/
     *
     * @return ViewModel
     *
     */
    public function doitAction(): ViewModel
    {
        $viewModel = new ViewModel();

        $viewModel->setVariable('code', $this->generateClassWithMethods());
        return $viewModel;
    }

    /**
     * https://zendframework.github.io/zend-code/generator/examples/#generating-php-classes-with-class-methods
     * @return string
     */
    protected function generateClassWithMethods(): string
    {
        $param = new ParameterGenerator();
        $param->setName('paramX')->setType('Bitkorn\MyType')->setPosition(1);

        $class = new ClassGenerator();
        $docblock = DocBlockGenerator::fromArray([
            'shortDescription' => 'Sample generated class',
            'longDescription' => 'This is a class generated with Laminas\Code\Generator.',
            'tags' => [
                [
                    'name' => 'version',
                    'description' => '$Rev:$',
                ],
                [
                    'name' => 'license',
                    'description' => 'New BSD',
                ],
            ],
        ]);
        $class->setName('Foo')
            ->setDocblock($docblock)
            ->addProperties([
                ['_bar', 'baz', PropertyGenerator::FLAG_PROTECTED],
                ['baz', 'bat', PropertyGenerator::FLAG_PUBLIC]
            ])
            ->addConstants([
                ['bat', 'foobarbazbat', PropertyGenerator::FLAG_CONSTANT]
            ])
            ->addMethods([
                // Method passed as array
                MethodGenerator::fromArray([
                    'name' => 'setBar',
                    'parameters' => [$param, ['name' => 'bar', 'type' => 'string', 'defaultvalue' => 'nix', 'position' => 2]],
                    'body' => '$this->_bar = $bar;' . "\n" . 'return \'toll\';',
                    'docblock' => DocBlockGenerator::fromArray([
                        'shortDescription' => 'Set the bar property',
                        'longDescription' => null,
                        'tags' => [
                            new ParamTag('paramX', [
                                'type' => 'Bitkorn\MyType'
                            ]),
                            new ParamTag('bar', [
                                'type' => 'string'
                            ]),
                            new ReturnTag([
                                'datatype' => 'string',
                            ]),
                        ],
                    ]),
                ])->setReturnType('string'),
                // Method passed as concrete instance
                new MethodGenerator(
                    'getBar',
                    [],
                    MethodGenerator::FLAG_PUBLIC,
                    'return $this->_bar;',
                    DocBlockGenerator::fromArray([
                        'shortDescription' => 'Retrieve the bar property',
                        'longDescription' => null,
                        'tags' => [
                            new ReturnTag([
                                'datatype' => 'string|null',
                            ]),
                        ],
                    ])
                ),
            ])->setNamespaceName('Bitkorn\Codegen')->addUse('Bitkorn\MyType');
        return $class->generate();
    }
}
