<?php

namespace Bitkorn\CodeCreate\Controller\Ajax\Common;

use Bitkorn\CodeCreate\Controller\AbstractHttpController;
use Laminas\View\Model\JsonModel;

class FilesystemAutoloadController extends AbstractHttpController
{
    /**
     * @var array
     */
    protected $autoloadPsr4 = [];

    /**
     * @return JsonModel
     */
    public function autocompleteFilesystemAction()
    {
        $jsonModel = new JsonModel();
        $paramModuleNamespace = $this->params()->fromQuery('moduleNs');
        $paramClassNamespace = $this->params()->fromQuery('classNs');
        if (!isset($paramModuleNamespace) || !isset($paramClassNamespace)) {
            return $jsonModel;
        }
        if (!isset($this->autoloadPsr4[$paramModuleNamespace])) {
            return $jsonModel;
        }

        $path = $this->autoloadPsr4[$paramModuleNamespace][0];
        if (!file_exists($path) || scandir($path) === false) {
            return $jsonModel;
        }

        $results = [];
        $classDir = $classDirBase = $path;
        $classNS = $classNSBase = '';

        if (($pathEntries = scandir($classDir)) === false) {
            return $jsonModel;
        }
        $allNS = [];
        foreach ($pathEntries as $pathEntry) {
            if (!$this->isDirname($pathEntry)) {
                continue;
            }
            $classDir .= DIRECTORY_SEPARATOR . $pathEntry;
            $classNS .= $pathEntry . '\\';
            if (file_exists($classDir) && is_dir($classDir)) {
                $allNS[] = $classNS;
                $this->fetchAllClassNS($allNS, $classDir, $classNS);
            }
            $classDir = $classDirBase;
            $classNS = $classNSBase;
        }

        foreach($allNS as $ns) {
            if (strlen(stristr($ns, $paramClassNamespace)) > 0) {
                $results[] = $ns;
            }
        }
        $jsonModel->setVariables($results);
        return $jsonModel;
    }

    /**
     * @param array $termResult
     * @param string $classDir
     * @param string $classNS
     */
    private function fetchAllClassNS(array &$termResult, string $classDir, string $classNS): void
    {
        if (!file_exists($classDir) || !is_dir($classDir)) {
            return;
        }
        $classDirBase = $classDir;
        $classNSBase = $classNS;
        $pathEntries = scandir($classDir);
        foreach ($pathEntries as $pathEntry) {
            if (!$this->isDirname($pathEntry)) {
                continue;
            }
            if (file_exists($classDir . DIRECTORY_SEPARATOR . $pathEntry) && is_dir($classDir . DIRECTORY_SEPARATOR . $pathEntry)) {
                $classDir .= DIRECTORY_SEPARATOR . $pathEntry;
                $classNS .= $pathEntry . '\\';
                $termResult[] = $classNS;
                $this->fetchAllClassNS($termResult, $classDir, $classNS);
            }
            $classDir = $classDirBase;
            $classNS = $classNSBase;
        }
    }

    private function isDirname(string $dirName): bool
    {
        if (strpos($dirName, '.') === false) {
            return true;
        }
        return false;
    }

    /**
     * @param string $autoloadPsr4
     */
    public function setAutoloadPsr4(string $autoloadPsr4): void
    {
        $this->autoloadPsr4 = include realpath($autoloadPsr4);
    }

}
