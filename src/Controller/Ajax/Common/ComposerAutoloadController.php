<?php


namespace Bitkorn\CodeCreate\Controller\Ajax\Common;


use Bitkorn\CodeCreate\Controller\AbstractHttpController;
use Laminas\View\Model\JsonModel;

class ComposerAutoloadController extends AbstractHttpController
{
    /**
     * @var array
     */
    protected $autoloadPsr4 = [];

    /**
     * @var array
     */
    protected $autoloadPsr4Keys = [];

    /**
     * @return JsonModel
     */
    public function autocompleteNamespaceAction()
    {
        $jsonModel = new JsonModel();
        $term = $this->params()->fromQuery('term');
        if(!isset($term)) {
            return $jsonModel;
        }
        $termResult = [];

        foreach ($this->autoloadPsr4 as $psr4namespace => $directories) {
            if(strlen(stristr($psr4namespace, $term)) > 0) {
                $obj = new \stdClass();
                $obj->label = $psr4namespace;
                $obj->value = $directories[0];
                $termResult[] = $obj;
            }
        }

        $jsonModel->setVariables($termResult);
        return $jsonModel;
    }

    /**
     * @param string $autoloadPsr4
     */
    public function setAutoloadPsr4(string $autoloadPsr4): void
    {
        $this->autoloadPsr4 = include realpath($autoloadPsr4);
        $this->autoloadPsr4Keys = array_keys($this->autoloadPsr4);
    }
}
