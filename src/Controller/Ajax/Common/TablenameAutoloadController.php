<?php

namespace Bitkorn\CodeCreate\Controller\Ajax\Common;

use Bitkorn\CodeCreate\Controller\AbstractHttpController;
use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\ParameterContainer;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\PhpEnvironment\Response;
use Laminas\View\Model\JsonModel;

class TablenameAutoloadController extends AbstractHttpController
{
    /**
     * @var Adapter
     */
    protected $adapter;

    protected $queryTablename = 'SELECT table_name FROM information_schema.tables WHERE table_schema = \'public\' AND table_name LIKE :like_ ORDER BY table_name ASC';

    /**
     * @param Adapter $adapter
     */
    public function setAdapter(Adapter $adapter): void
    {
        $this->adapter = $adapter;
    }

    /**
     * @return JsonModel
     */
    public function autocompleteTablenameAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        $tablename = $this->params()->fromQuery('tablename');

        $qParams = new ParameterContainer(['like_' => $tablename . '%']);
        $stmt = $this->adapter->createStatement($this->queryTablename, $qParams);
        $result = $stmt->execute();
        if (!$result->valid()) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        $tablenames = [];
        do {
            $tablenames[] = $result->current()['table_name'];
            $result->next();
        } while ($result->valid());
        $jsonModel->setVariables($tablenames);
        return $jsonModel;
    }
}
