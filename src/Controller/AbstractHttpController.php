<?php


namespace Bitkorn\CodeCreate\Controller;


use Laminas\Log\Logger;
use Laminas\Mvc\Controller\AbstractActionController;

class AbstractHttpController extends AbstractActionController
{
    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @param Logger $logger
     */
    public function setLogger(Logger $logger): void
    {
        $this->logger = $logger;
    }
    
}
