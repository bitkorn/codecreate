<?php

namespace Bitkorn\CodeCreate\Controller\Create;

use Bitkorn\CodeCreate\Controller\AbstractHttpController;
use Bitkorn\CodeCreate\Form\Create\BasicForm;
use Bitkorn\CodeCreate\Service\Create\Type\CreateTypeService;
use Bitkorn\CodeCreate\Service\Create\Type\MappingGetterSetterService;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\PhpEnvironment\Response;
use Laminas\View\Model\ViewModel;

class EntityController extends AbstractHttpController
{
    protected MappingGetterSetterService $mappingGetterSetterService;
    protected BasicForm $basicForm;

    public function setMappingGetterSetterService(MappingGetterSetterService $mappingGetterSetterService): void
    {
        $this->mappingGetterSetterService = $mappingGetterSetterService;
    }

    public function setBasicForm(BasicForm $basicForm): void
    {
        $this->basicForm = $basicForm;
    }

    /**
     * @return ViewModel
     */
    public function createEntityAction()
    {
        $viewModel = new ViewModel();
        $this->basicForm->setEntityType(true);
        $this->basicForm->init();

        $request = $this->getRequest();
        if ($request instanceof Request && $request->isPost()) {
            $postData = $request->getPost();
            $this->basicForm->setData($postData);
            if ($this->basicForm->isValid()) {
                $formData = $this->basicForm->getData();
                $this->mappingGetterSetterService->setNamespaceModule($formData['namespace_module']);
                $this->mappingGetterSetterService->setNamespaceClass($formData['namespace_class']);
                $this->mappingGetterSetterService->setDirectorySrc($formData['directory_src']);
                $this->mappingGetterSetterService->setClassname($formData['classname']);
                $this->mappingGetterSetterService->setTableName($formData['table_name']);
                if(empty($formData['with_getter'])) {
                    $this->mappingGetterSetterService->setWithGetter(false);
                }
                if(empty($formData['with_setter'])) {
                    $this->mappingGetterSetterService->setWithSetter(false);
                }
                if(!empty($formData['camel_case'])) {
                    $this->mappingGetterSetterService->setCamelCase(true);
                }
                if(!empty($formData['column_names'])) {
                    $columnNamesFlat = explode(',', $formData['column_names']);
                    $columnNames = [];
                    foreach($columnNamesFlat as $columnNameFlat) {
                        $expldt = explode('=', $columnNameFlat);
                        $columnNames[trim($expldt[0])] = trim($expldt[1]);
                    }
                    $this->mappingGetterSetterService->setColumnNames($columnNames);
                }
                $viewModel->setVariable('success', $this->mappingGetterSetterService->createEntity());
            }
        }

        $viewModel->setVariable('basicForm', $this->basicForm);

        $this->layout('layout/layout-code-create');

        return $viewModel;
    }
}
