<?php


namespace Bitkorn\CodeCreate\Controller\Create;


use Bitkorn\CodeCreate\Controller\AbstractHttpController;
use Bitkorn\CodeCreate\Form\Create\BasicForm;
use Bitkorn\CodeCreate\Form\Create\ModuleForm;
use Bitkorn\CodeCreate\Service\Create\Module\CreateModuleService;
use Bitkorn\CodeCreate\Service\Create\Type\CreateTypeService;
use Bitkorn\Trinket\Filter\FilterChainStringSanitize;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\View\Model\ViewModel;

class BasicController extends AbstractHttpController
{
    protected CreateTypeService $createTypeService;
    protected CreateModuleService $createModuleService;
    protected BasicForm $basicForm;
    protected ModuleForm $moduleForm;

    public function setCreateTypeService(CreateTypeService $createTypeService): void
    {
        $this->createTypeService = $createTypeService;
    }

    public function setCreateModuleService(CreateModuleService $createModuleService): void
    {
        $this->createModuleService = $createModuleService;
    }

    public function setBasicForm(BasicForm $basicForm): void
    {
        $this->basicForm = $basicForm;
    }

    public function setModuleForm(ModuleForm $moduleForm): void
    {
        $this->moduleForm = $moduleForm;
    }

    /**
     * @return ViewModel
     */
    public function createTypesAction()
    {
        $viewModel = new ViewModel();
        $this->basicForm->init();

        $request = $this->getRequest();
        if ($request instanceof Request && $request->isPost()) {
            $postData = $request->getPost();
            $this->basicForm->setData($postData);
            if ($this->basicForm->isValid()) {
                $formData = $this->basicForm->getData();
                $this->createTypeService->setNamespaceModule($formData['namespace_module']);
                $this->createTypeService->setNamespaceClass($formData['namespace_class']);
                $this->createTypeService->setDirectorySrc($formData['directory_src']);
                $this->createTypeService->setClassname($formData['classname']);
                $this->createTypeService->setType($formData['codecreate_type']);
                if(!empty($formData['table_name'])) {
                    $this->createTypeService->setTableName($formData['table_name']);
                }
                $this->createTypeService->setOnlyFactory($formData['only_factory'] == 1);
                $viewModel->setVariable('success', $this->createTypeService->createType());
            }
        }

        $viewModel->setVariable('autoloadPsr4', $this->createTypeService->getAutoloadPsr4());
        $viewModel->setVariable('basicForm', $this->basicForm);

        $this->layout('layout/layout-code-create');

        return $viewModel;
    }

    /**
     * @return ViewModel
     */
    public function createModuleAction()
    {
        $viewModel = new ViewModel();
        $this->moduleForm->init();

        $request = $this->getRequest();
        if ($request instanceof Request && $request->isPost()) {
            $postData = $request->getPost();
            $this->moduleForm->setData($postData);
            if ($this->moduleForm->isValid()) {
                $formData = $this->moduleForm->getData();
                $moduleNamespace = $formData['module_namespace'];
                $this->createModuleService->setNamespace($moduleNamespace);
                $viewModel->setVariable('success', $this->createModuleService->createModule());
            }
        }

        $this->layout('layout/layout-code-create');

        $viewModel->setVariable('moduleForm', $this->moduleForm);
        return $viewModel;
    }

}
