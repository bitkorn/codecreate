<?php


namespace Bitkorn\CodeCreate\Types\Classes\Service;


use Bitkorn\CodeCreate\Types\Classes\AbstractClassType;
use Bitkorn\Trinket\Service\AbstractService;
use Bitkorn\Trinket\Service\Filesystem\FolderTool;
use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;
use Laminas\Log\Logger;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ServiceType extends AbstractClassType
{

    /**
     * @param FolderTool $folderTool
     * @return bool
     */
    public function createType(FolderTool $folderTool): bool
    {
        $filePath = $this->directory . '/' . $this->classname . '.php';

        $this->phpNamespace->addUse(AbstractService::class);
        $this->phpNamespace->addUse(Logger::class);

        $class = $this->phpNamespace->addClass($this->classname);
        $class->setExtends(AbstractService::class);

        return $this->filePutContents($filePath, $this->file) && $folderTool->chmodFile($filePath);
    }
}
