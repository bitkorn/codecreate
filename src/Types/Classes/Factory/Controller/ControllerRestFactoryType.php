<?php


namespace Bitkorn\CodeCreate\Types\Classes\Factory\Controller;


use Bitkorn\CodeCreate\Types\Classes\AbstractClassType;
use Bitkorn\CodeCreate\Types\Classes\Factory\AbstractFactoryType;
use Bitkorn\Trinket\Service\Filesystem\FolderTool;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpNamespace;
use Laminas\Code\Generator\DocBlockGenerator;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ControllerRestFactoryType extends AbstractFactoryType
{

    /**
     * @param FolderTool $folderTool
     * @return bool
     */
    public function createType(FolderTool $folderTool): bool
    {
        $filePath = $this->directory . '/' . $this->classname . '.php';

        $this->phpNamespace->addUse(ContainerInterface::class);
        $this->phpNamespace->addUse(ServiceNotCreatedException::class);
        $this->phpNamespace->addUse(ServiceNotFoundException::class);
        $this->phpNamespace->addUse(FactoryInterface::class);
        $this->phpNamespace->addUse(UserService::class);
        $this->phpNamespace->addUse($this->classnameCompleteMake);

        $class = $this->phpNamespace->addClass($this->classname);
        $class->addImplement(FactoryInterface::class);

        $method = $class->addMethod('__invoke');
        $method->addComment('Create an object');
        $method->addComment('');
        $method->addComment('@param ContainerInterface $container');
        $method->addComment('@param string $requestedName');
        $method->addComment('@param null|array $options');
        $method->addComment('@return object');
        $method->addComment('@throws ServiceNotFoundException if unable to resolve the service');
        $method->addComment('@throws ServiceNotCreatedException if an exception is raised when creating a service');
//        $method->addComment('@throws ContainerException if any other error occurs');
        $method->addParameter('container')
            ->setType(ContainerInterface::class);
        $method->addParameter('requestedName');
        $method->addParameter('options')
            ->setType('array')
            ->setDefaultValue(null);
        $method->setBody('$controller = new ' . $this->classnameMake . '();'
            . "\n" . '$controller->setLogger($container->get(\'logger\'));'
            . "\n" . '$controller->setUserService($container->get(UserService::class));'
            . "\n" . 'return $controller;');

        return $this->filePutContents($filePath, $this->file) && $folderTool->chmodFile($filePath);
    }

}
