<?php


namespace Bitkorn\CodeCreate\Types\Classes\Factory;


use Bitkorn\CodeCreate\Types\Classes\AbstractClassType;

abstract class AbstractFactoryType extends AbstractClassType
{


    /**
     * @var string
     */
    protected $classnameCompleteMake = '';

    /**
     * @var string
     */
    protected $classnameMake = '';

    /**
     * @param string $classnameCompleteMake
     */
    public function setClassnameCompleteMake(string $classnameCompleteMake): void
    {
        $this->classnameCompleteMake = $classnameCompleteMake;
        $this->classnameMake = substr($classnameCompleteMake, strrpos($classnameCompleteMake, '\\') + 1);
    }
}
