<?php

namespace Bitkorn\CodeCreate\Types\Classes\Factory\Command;

use Bitkorn\CodeCreate\Types\Classes\Factory\AbstractFactoryType;
use Bitkorn\Trinket\Service\Filesystem\FolderTool;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class CommandSymfonyFactoryType extends AbstractFactoryType
{

    /**
     * @inheritDoc
     */
    public function createType(FolderTool $folderTool): bool
    {
        $filePath = $this->directory . '/' . $this->classname . '.php';

        $this->phpNamespace->addUse(ContainerInterface::class);
        $this->phpNamespace->addUse(ServiceNotCreatedException::class);
        $this->phpNamespace->addUse(ServiceNotFoundException::class);
        $this->phpNamespace->addUse(FactoryInterface::class);
        $this->phpNamespace->addUse($this->classnameCompleteMake);

        $class = $this->phpNamespace->addClass($this->classname);
        $class->addImplement(FactoryInterface::class);

        $method = $class->addMethod('__invoke');
        $method->addComment('Create an object');
        $method->addComment('');
        $method->addComment('@param ContainerInterface $container');
        $method->addComment('@param string $requestedName');
        $method->addComment('@param null|array $options');
        $method->addComment('@return object');
        $method->addComment('@throws ServiceNotFoundException if unable to resolve the service');
        $method->addComment('@throws ServiceNotCreatedException if an exception is raised when creating a service');
        $method->addParameter('container')
            ->setType(ContainerInterface::class);
        $method->addParameter('requestedName');
        $method->addParameter('options')
            ->setType('array')
            ->setDefaultValue(null);
        $method->setBody('$command = new ' . $this->classnameMake . '();'
            . "\n" . '$command->setLogger($container->get(\'logger\'));'
            . "\n" . 'return $command;');

        return $this->filePutContents($filePath, $this->file) && $folderTool->chmodFile($filePath);
    }
}
