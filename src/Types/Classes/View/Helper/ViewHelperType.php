<?php


namespace Bitkorn\CodeCreate\Types\Classes\View\Helper;


use Bitkorn\CodeCreate\Types\Classes\AbstractClassType;
use Bitkorn\Trinket\Service\Filesystem\FolderTool;
use Bitkorn\Trinket\View\Helper\AbstractViewHelper;
use Laminas\Log\Logger;
use Laminas\View\Model\ViewModel;

class ViewHelperType extends AbstractClassType
{

    /**
     * @param FolderTool $folderTool
     * @return bool
     */
    public function createType(FolderTool $folderTool): bool
    {
        $filePath = $this->directory . '/' . $this->classname . '.php';

        $this->phpNamespace->addUse(AbstractViewHelper::class);
        $this->phpNamespace->addUse(Logger::class);
        $this->phpNamespace->addUse(ViewModel::class);

        $class = $this->phpNamespace->addClass($this->classname);
        $class->setExtends(AbstractViewHelper::class);

        $class->addConstant('TEMPLATE', 'template/someTamplate');

        $method = $class->addMethod('__invoke');
        $method->addComment('@param string $uuid');
        $method->addComment('@return string');
        $method->addParameter('uuid')->setType('string');

        $method->setBody('if(empty($uuid)) {
    return \'\';
}
$viewModel = new ViewModel();
return $this->getView()->render($viewModel);');

        return $this->filePutContents($filePath, $this->file) && $folderTool->chmodFile($filePath);
    }
}
