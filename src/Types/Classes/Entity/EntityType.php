<?php


namespace Bitkorn\CodeCreate\Types\Classes\Entity;


use Bitkorn\CodeCreate\Types\Classes\AbstractClassType;
use Bitkorn\CodeCreate\Types\Functions\Entity\Getter;
use Bitkorn\CodeCreate\Types\Functions\Entity\Setter;
use Bitkorn\Trinket\Entity\AbstractEntity;
use Bitkorn\Trinket\Service\Filesystem\FolderTool;
use Nette\PhpGenerator\Helpers;
use Nette\PhpGenerator\PhpLiteral;
use Laminas\Log\Logger;

class EntityType extends AbstractClassType
{
    protected Logger $logger;

    /**
     * @var array [columnName => dataType]
     */
    protected $columnNames = [];

    /**
     * @var array [dbColumnName => camelCaseName]
     */
    protected $mapping = [];

    protected bool $withGetter = true;
    protected bool $withSetter = true;
    protected bool $camelCase = false;

    public function setLogger(Logger $logger): void
    {
        $this->logger = $logger;
    }

    /**
     * @param array $columnNames [columnName => dataType]
     */
    public function setColumnNames(array $columnNames): void
    {
        $this->columnNames = $columnNames;
    }

    public function setWithGetter(bool $withGetter): void
    {
        $this->withGetter = $withGetter;
    }

    public function setWithSetter(bool $withSetter): void
    {
        $this->withSetter = $withSetter;
    }

    public function setCamelCase(bool $camelCase): void
    {
        $this->camelCase = $camelCase;
    }

    /**
     * @param FolderTool $folderTool
     * @return bool
     */
    public function createType(FolderTool $folderTool): bool
    {
        $filePath = $this->directory . DIRECTORY_SEPARATOR . $this->classname . '.php';
        $this->phpNamespace->addUse(AbstractEntity::class);
        $class = $this->phpNamespace->addClass($this->classname);
        $class->setExtends(AbstractEntity::class);

        foreach ($this->columnNames as $columnName => $columnType) {
            $camelCase = lcfirst(str_replace('_', '', ucwords($columnName, '_')));
            if($this->camelCase) {
                $this->mapping[$columnName] = $camelCase;
            } else {
                $this->mapping[$columnName] = $columnName;
            }
            if ($this->withGetter) {
                $method = $class->addMethod(new Getter($camelCase));
                $method->setReturnType($this->switchColumnDataType($columnType));
                $method->setBody(
                    'if(!isset($this->storage[\'' . $columnName . '\'])) {'
                    . "\n\t" . 'return ' . $this->switchColumnDataTypeValue($columnType) . ';'
                    . "\n" . '}'
                    . "\n" . 'return $this->storage[\'' . $columnName . '\'];');
            }
            if ($this->withSetter) {
                $method = $class->addMethod(new Setter($camelCase));
                $param = $method->addParameter($camelCase);
                $param->setType($this->switchColumnDataType($columnType));
                $method->setReturnType('void');
                $method->setBody('$this->storage[\'' . $columnName . '\'] = $' . $camelCase . ';');
            }
        }
        $prop = $class->addProperty('mapping', $this->mapping);
        $prop->setType('array');
        return $this->filePutContents($filePath, $this->file) && $folderTool->chmodFile($filePath);
    }

    protected function switchColumnDataType(string $type): string
    {
        switch ($type) {
            case 'array': // custom
                $s = 'array';
                break;
            case 'int': // MySQL
            case 'bigint': // MySQL
            case 'int4':
            case 'int8':
                $s = 'int';
                break;
            case 'decimal': // MySQL
            case 'float4':
            case 'numeric':
            case 'real':
                $s = 'float';
                break;
            case 'text':
            case 'varchar':
            case 'uuid':
            case 'timestamp':
            case 'char':
            case 'date':
                $s = 'string';
                break;
            case 'bool':
                $s = 'bool';
                break;
            default:
                if (strpos($type, 'enum_') === 0) {
                    $s = 'string';
                    break;
                } else {
                    $s = '';
                }
        }
        return $s;
    }

    protected function switchColumnDataTypeAtReturn(string $type, string $name): string
    {
        $s = '@return ';
        switch ($type) {
            case 'int4':
            case 'int8':
                $s .= 'int';
                break;
            case 'float4':
            case 'real':
                $s .= 'float';
                break;
            case 'text':
            case 'varchar':
            case 'uuid':
            case 'timestamp':
            case 'char':
                $s .= 'string';
                break;
            case 'bool':
                $s .= 'bool';
                break;
            default:
                if (strpos($type, 'enum_') === 0) {
                    $s .= 'string';
                    break;
                } else {
                    $s = '';
                }
        }
        return $s;
    }

    protected function switchColumnDataTypeValue(string $type): string
    {
        switch ($type) {
            case 'int4':
            case 'int8':
            case 'float4':
                return '0';
            default:
                return '\'\'';
        }
    }
}
