<?php

namespace Bitkorn\CodeCreate\Types\Classes;

use Bitkorn\CodeCreate\Types\AbstractPhpFile;
use Bitkorn\Trinket\Service\Filesystem\FolderTool;

abstract class AbstractClassType extends AbstractPhpFile
{

    /**
     * @var string
     */
    protected $classname;

    /**
     * @param string $classname
     */
    public function setClassname(string $classname): void
    {
        $this->classname = $classname;
    }

    /**
     * @param FolderTool $folderTool
     * @return bool
     */
    public abstract function createType(FolderTool $folderTool): bool;
}
