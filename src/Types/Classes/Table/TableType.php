<?php

namespace Bitkorn\CodeCreate\Types\Classes\Table;

use Bitkorn\CodeCreate\Types\Classes\AbstractClassType;
use Bitkorn\CodeCreate\Types\Functions\Entity\Getter;
use Bitkorn\Trinket\Service\Filesystem\FolderTool;
use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Predicate\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class TableType extends AbstractClassType
{

    protected string $tableName = '';

    /**
     * @param FolderTool $folderTool
     * @return bool
     */
    public function createType(FolderTool $folderTool): bool
    {
        $filePath = $this->directory . '/' . $this->classname . '.php';
        $camelCase = lcfirst(str_replace('_', '', ucwords($this->tableName, '_')));

        $this->phpNamespace->addUse(AbstractLibTable::class);
        $this->phpNamespace->addUse(HydratingResultSet::class);
        $this->phpNamespace->addUse(Expression::class);
        $this->phpNamespace->addUse(Select::class);
        $this->phpNamespace->addUse(Update::class);
        $this->phpNamespace->addUse(Delete::class);
        $this->phpNamespace->addUse(Where::class);

        $class = $this->phpNamespace->addClass($this->classname);
        $class->setExtends(AbstractLibTable::class);

        $class->addProperty('table', $this->tableName)->addComment('@var string')->setVisibility('protected');
        $paramName = '$' . $camelCase . 'Uuid';
        $method = $class->addMethod(new Getter(ucfirst($camelCase)));
        $method->addComment('@param string ' . $paramName);
        $method->addComment('@return array');
        $method->addParameter(substr($paramName, 1))->setType('string');
        $method->setReturnType('array');

        $method->setBody('$select = $this->sql->select();
try {
    $select->where([\'' . $this->tableName . '_uuid\' => ' . $paramName . ']);
    /** @var HydratingResultSet $result */
    $result = $this->selectWith($select);
    if ($result->valid() && $result->count() == 1) {
        return $result->toArray()[0];
    }
} catch (\Exception $exception) {
    $this->log($exception, __CLASS__, __FUNCTION__);
}
return [];');

        return $this->filePutContents($filePath, $this->file) && $folderTool->chmodFile($filePath);
    }

    /**
     * @param string $tableName
     */
    public function setTableName(string $tableName): void
    {
        $this->tableName = $tableName;
    }

}
