<?php

namespace Bitkorn\CodeCreate\Types\Classes\Controller;

use Bitkorn\CodeCreate\Types\Classes\AbstractClassType;
use Bitkorn\CodeCreate\Types\Functions\Controller\Rest\ControllerActionRest;
use Bitkorn\Trinket\Service\Filesystem\FolderTool;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Http\Response;
use Bitkorn\Trinket\View\Model\JsonModel;

class ControllerRestType extends AbstractClassType
{

    /**
     * @var ControllerActionRest[]
     */
    protected array $actionFunctions = [];

    /**
     * @param ControllerActionRest $controllerActionRest
     */
    public function addActionFunction(ControllerActionRest $controllerActionRest): void
    {
        $this->actionFunctions[] = $controllerActionRest;
    }

    /**
     * @param FolderTool $folderTool
     * @return bool
     */
    public function createType(FolderTool $folderTool): bool
    {
        $filePath = $this->directory . '/' . $this->classname . '.php';

        $this->phpNamespace->addUse(AbstractUserRestController::class);
        $this->phpNamespace->addUse(Response::class);
        $this->phpNamespace->addUse(JsonModel::class);

        $class = $this->phpNamespace->addClass($this->classname);
        $class->setExtends(AbstractUserRestController::class);

        $restMethodBody = '$jsonModel = new ' . $this->phpNamespace->unresolveName(JsonModel::class) . "();\n" . '$this->getResponse()->setStatusCode(Response::STATUS_CODE_501);' . "\n" . 'return $jsonModel;';

        $method = $class->addMethod('create');
        $method->addComment('POST maps to create().');
        $method->setBody($restMethodBody);
        $method->setReturnType(JsonModel::class);
        $method->addParameter('data');
        $method->addComment('@param array $data');
        $method->addComment('@return ' . $this->phpNamespace->unresolveName(JsonModel::class));

        $method = $class->addMethod('delete');
        $method->addComment('DELETE maps to delete().');
        $method->setBody($restMethodBody);
        $method->setReturnType(JsonModel::class);
        $method->addParameter('id');
        $method->addComment('@param string $id');
        $method->addComment('@return ' . $this->phpNamespace->unresolveName(JsonModel::class));

        $method = $class->addMethod('get');
        $method->addComment('GET');
        $method->setBody($restMethodBody);
        $method->setReturnType(JsonModel::class);
        $method->addParameter('id');
        $method->addComment('@param string $id');
        $method->addComment('@return ' . $this->phpNamespace->unresolveName(JsonModel::class));

        $method = $class->addMethod('getList');
        $method->addComment('GET');
        $method->setBody($restMethodBody);
        $method->setReturnType(JsonModel::class);
        $method->addComment('@return ' . $this->phpNamespace->unresolveName(JsonModel::class));

        $method = $class->addMethod('update');
        $method->addComment('PUT maps to update().');
        $method->setBody($restMethodBody);
        $method->setReturnType(JsonModel::class);
        $method->addParameter('id');
        $method->addParameter('data');
        $method->addComment('@param string $id');
        $method->addComment('@param array $data');
        $method->addComment('@return ' . $this->phpNamespace->unresolveName(JsonModel::class));

//        foreach ($this->actionFunctions as $actionFunction) {
//            $method = $class->addMethod($actionFunction->getFunctionName());
//            $method->setBody('$jsonModel = new ' . $this->phpNamespace->unresolveName(JsonModel::class) . "();\n\n" . 'return $jsonModel;');
//            $method->setReturnType(JsonModel::class);
//            foreach ($actionFunction->getParameters() as $parameter) {
//                $method->addParameter($parameter->getName());
//                $method->addComment('@param ' . $parameter->getTypeHint() . ' $' . $parameter->getName());
//            }
//            $method->addComment('@return ' . $this->phpNamespace->unresolveName(JsonModel::class));
//        }

        return $this->filePutContents($filePath, $this->file) && $folderTool->chmodFile($filePath);
    }
}
