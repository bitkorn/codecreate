<?php

namespace Bitkorn\CodeCreate\Types\Classes\Controller;

use Bitkorn\CodeCreate\Types\Classes\AbstractClassType;
use Bitkorn\CodeCreate\Types\Functions\Controller\Html\ControllerActionHtml;
use Bitkorn\Trinket\Service\Filesystem\FolderTool;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\View\Model\ViewModel;

class ControllerHtmlType extends AbstractClassType
{

    /**
     * @var ControllerActionHtml[]
     */
    protected array $actionFunctions = [];

    /**
     * @param ControllerActionHtml $controllerActionHtml
     */
    public function addActionFunction(ControllerActionHtml $controllerActionHtml): void
    {
        $this->actionFunctions[] = $controllerActionHtml;
    }

    /**
     * @param FolderTool $folderTool
     * @return bool
     */
    public function createType(FolderTool $folderTool): bool
    {
        $filePath = $this->directory . DIRECTORY_SEPARATOR . $this->classname . '.php';

        $this->phpNamespace->addUse(AbstractUserController::class);
        $this->phpNamespace->addUse(ViewModel::class);

        $class = $this->phpNamespace->addClass($this->classname);
        $class->setExtends(AbstractUserController::class);

        foreach ($this->actionFunctions as $actionFunction) {
            $method = $class->addMethod($actionFunction);
            $method->addComment('@return ' . $this->phpNamespace->unresolveName(ViewModel::class));
            $method->setBody('$viewModel = new ' . $this->phpNamespace->unresolveName(ViewModel::class) . "();\n\n" . 'return $viewModel;');
        }

        return $this->filePutContents($filePath, $this->file) && $folderTool->chmodFile($filePath);
    }
}
