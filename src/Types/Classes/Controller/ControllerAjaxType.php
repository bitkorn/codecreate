<?php

namespace Bitkorn\CodeCreate\Types\Classes\Controller;

use Bitkorn\CodeCreate\Types\Classes\AbstractClassType;
use Bitkorn\CodeCreate\Types\Functions\Controller\Json\ControllerActionAjax;
use Bitkorn\Trinket\Service\Filesystem\FolderTool;
use Bitkorn\User\Controller\AbstractUserController;
use Bitkorn\Trinket\View\Model\JsonModel;

class ControllerAjaxType extends AbstractClassType
{

    /**
     * @var ControllerActionAjax[]
     */
    protected array $actionFunctions = [];

    /**
     * @param ControllerActionAjax $controllerActionJson
     */
    public function addActionFunction(ControllerActionAjax $controllerActionJson): void
    {
        $this->actionFunctions[] = $controllerActionJson;
    }

    /**
     * @param FolderTool $folderTool
     * @return bool
     */
    public function createType(FolderTool $folderTool): bool
    {
        $filePath = $this->directory . DIRECTORY_SEPARATOR . $this->classname . '.php';

        $this->phpNamespace->addUse(AbstractUserController::class);
        $this->phpNamespace->addUse(JsonModel::class);

        $class = $this->phpNamespace->addClass($this->classname);
        $class->setExtends(AbstractUserController::class);

        foreach ($this->actionFunctions as $actionFunction) {
            $method = $class->addMethod($actionFunction);
            $method->addComment('@return ' . $this->phpNamespace->unresolveName(JsonModel::class));
            $method->setBody('$jsonModel = new ' . $this->phpNamespace->unresolveName(JsonModel::class) . "();\n\n" . 'return $jsonModel;');
        }

        return $this->filePutContents($filePath, $this->file) && $folderTool->chmodFile($filePath);
    }
}
