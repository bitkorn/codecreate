<?php


namespace Bitkorn\CodeCreate\Types\Classes\Form;


use Bitkorn\CodeCreate\Types\Classes\AbstractClassType;
use Bitkorn\Trinket\Form\AbstractForm;
use Bitkorn\Trinket\Service\AbstractService;
use Bitkorn\Trinket\Service\Filesystem\FolderTool;
use Laminas\Filter\HtmlEntities;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Log\Logger;
use Laminas\Validator\StringLength;
use Laminas\View\Model\JsonModel;

class FormType extends AbstractClassType
{

    /**
     * @param FolderTool $folderTool
     * @return bool
     */
    public function createType(FolderTool $folderTool): bool
    {
        $filePath = $this->directory . '/' . $this->classname . '.php';

        $this->phpNamespace->addUse(AbstractForm::class);
        $this->phpNamespace->addUse(InputFilterProviderInterface::class);
        $this->phpNamespace->addUse(StringTrim::class);
        $this->phpNamespace->addUse(HtmlEntities::class);
        $this->phpNamespace->addUse(StripTags::class);
        $this->phpNamespace->addUse(StringLength::class);

        $class = $this->phpNamespace->addClass($this->classname);
        $class->setExtends(AbstractForm::class);
        $class->addImplement(InputFilterProviderInterface::class);

        $method = $class->addMethod('init');
        $method->setBody('$this->add([\'name\' => \'field_name\']);');

        $method = $class->addMethod('getInputFilterSpecification');
        $method->addComment('Should return an array specification compatible with');
        $method->addComment('{@link \\Laminas\InputFilter\Factory::createInputFilter()}.');
        $method->addComment('@return array');
        $method->setBody('$filter = [];
        
$filter[\'field_name\'] = [
    \'required\' => true,
    \'filters\' => [
        [\'name\' => StringTrim::class],
        [\'name\' => StripTags::class],
        [\'name\' => HtmlEntities::class],
    ], \'validators\' => [
        [
            \'name\' => StringLength::class,
            \'options\' => [
                \'encoding\' => \'UTF-8\',
                \'min\' => 2,
                \'max\' => 100,
            ]
        ]
    ]
];

return $filter;'
        );

        return $this->filePutContents($filePath, $this->file) && $folderTool->chmodFile($filePath);
    }
}
