<?php

namespace Bitkorn\CodeCreate\Types\Classes\Command;

use Bitkorn\CodeCreate\Types\Classes\AbstractClassType;
use Bitkorn\Trinket\Service\Filesystem\FolderTool;
use Laminas\Log\Logger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CommandSymfonyType extends AbstractClassType
{

    /**
     * @inheritDoc
     */
    public function createType(FolderTool $folderTool): bool
    {
        $filePath = $this->directory . DIRECTORY_SEPARATOR . $this->classname . '.php';

        $this->phpNamespace->addUse(Command::class);
        $this->phpNamespace->addUse(Logger::class);
        $this->phpNamespace->addUse(InputArgument::class);
        $this->phpNamespace->addUse(InputInterface::class);
        $this->phpNamespace->addUse(OutputInterface::class);

        $class = $this->phpNamespace->addClass($this->classname);
        $class->setExtends(Command::class);

        $class->addProperty('logger')->setType(Logger::class)->setVisibility('protected');

        $methodLogger = $class->addMethod('setLogger');
        $methodLogger->addParameter('logger')->setType(Logger::class);
        $methodLogger->setBody('$this->logger = $logger;');

        $methodConf = $class->addMethod('configure');
        $methodConf->setBody('$this->addArgument(\'id\', InputArgument::REQUIRED, \'Some ID\');');

        $methodExecute = $class->addMethod('execute');
        $methodExecute->addParameter('input')->setType(InputInterface::class);
        $methodExecute->addParameter('output')->setType(OutputInterface::class);
        $methodExecute->setBody('$id = $input->getArgument(\'id\');' . "\n" . '$output->writeln(\'Some ID: \' . $id);' . "\n" . 'return 1;');

        return $this->filePutContents($filePath, $this->file) && $folderTool->chmodFile($filePath);
    }
}
