<?php


namespace Bitkorn\CodeCreate\Types\Classes;

use Bitkorn\Trinket\Service\Filesystem\FolderTool;
use Laminas\ModuleManager\Feature\ConfigProviderInterface;
use Laminas\ModuleManager\Feature\ControllerProviderInterface;
use Laminas\ModuleManager\Feature\ServiceProviderInterface;
use Laminas\ModuleManager\Feature\ViewHelperProviderInterface;
use Laminas\View\Model\ViewModel;

class ModuleClassType extends AbstractClassType
{

    /**
     * @param FolderTool $folderTool
     * @return bool
     */
    public function createType(FolderTool $folderTool): bool
    {
        $filePath = $this->directory . '/' . $this->classname . '.php';

        $this->phpNamespace->addUse(ConfigProviderInterface::class);
        $this->phpNamespace->addUse(ServiceProviderInterface::class);
        $this->phpNamespace->addUse(ControllerProviderInterface::class);
        $this->phpNamespace->addUse(ViewHelperProviderInterface::class);

        $class = $this->phpNamespace->addClass($this->classname);
        $class->setImplements([
            ConfigProviderInterface::class,
            ServiceProviderInterface::class,
            ControllerProviderInterface::class,
            ViewHelperProviderInterface::class
        ]);

        $method = $class->addMethod('getConfig');
        $method->addComment('Returns configuration to merge with application configuration');
        $method->addComment('@return array|\Traversable');
        $method->setBody('return include __DIR__ . \'/../config/module.config.php\';');

        $method = $class->addMethod('getServiceConfig');
        $method->addComment('Expected to return \Laminas\ServiceManager\Config object or array to seed such an object.');
        $method->addComment('@return array|\Laminas\ServiceManager\Config');
        $method->setBody("return [\n\t'factories' => [\n\t]\n];");

        $method = $class->addMethod('getControllerConfig');
        $method->addComment('Expected to return \Laminas\ServiceManager\Config object or array to seed such an object.');
        $method->addComment('@return array|\Laminas\ServiceManager\Config');
        $method->setBody("return [\n\t'factories' => [\n\t]\n];");

        $method = $class->addMethod('getViewHelperConfig');
        $method->addComment('Expected to return \Laminas\ServiceManager\Config object or array to seed such an object.');
        $method->addComment('@return array|\Laminas\ServiceManager\Config');
        $method->setBody("return [\n];");

        return $this->filePutContents($filePath, $this->file) && $folderTool->chmodFile($filePath);
    }
}
