<?php


namespace Bitkorn\CodeCreate\Types;


use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\PhpNamespace;

class AbstractPhpFile
{
    /**
     * @var PhpFile
     */
    protected $file;

    /**
     * @var string
     */
    protected $namespace;

    /**
     * @var PhpNamespace
     */
    protected $phpNamespace;

    /**
     * @var string
     */
    protected $directory;

    /**
     * @var string
     */
    protected $filePath;

    /**
     * @var array
     */
    protected $uses = [];

    /**
     * AbstractClassType constructor.
     */
    public function __construct()
    {
        $this->file = new PhpFile();
    }

    /**
     * @return PhpFile
     */
    public function getFile(): PhpFile
    {
        return $this->file;
    }

    /**
     * @param string $directory
     */
    public function setDirectory(string $directory): void
    {
        $this->directory = $directory;
    }

    /**
     * @param string $filePath
     */
    public function setFilePath(string $filePath): void
    {
        $this->filePath = $filePath;
    }

    /**
     * @param string $namespace
     */
    public function setNamespace(string $namespace): void
    {
        $this->namespace = $namespace;
        $this->phpNamespace = $this->file->addNamespace($this->namespace);
    }

    /**
     * @param array $uses
     */
    public function setUses(array $uses): void
    {
        $this->uses = $uses;
    }

    /**
     * @param string $use
     */
    public function addUse(string $use): void
    {
        $this->uses[] = $use;
    }

    /**
     * @param string $filePath
     * @param PhpFile $file
     * @return bool
     */
    protected function filePutContents(string $filePath, PhpFile $file): bool
    {
        if(file_exists($filePath)){
            return false;
        }
        return is_int(file_put_contents($filePath, $file));
    }
}
