<?php

namespace Bitkorn\CodeCreate\Types\ModuleConfig;

use Laminas\Config\Writer\PhpArray as ZendPhpArray;

class PhpArray extends ZendPhpArray
{

    /**
     * @var string
     */
    protected $namespaceName = '';

    /**
     * @var array
     */
    protected $uses = [];

    /**
     * @param array $uses
     */
    public function setUses(array $uses): void
    {
        $this->uses = $uses;
    }

    /**
     * Attempts to convert a FQN string to class name scalar.
     * Returns false if string is not a valid FQN or can not be resolved to an existing name.
     *
     * @param string $string
     * @return bool|string
     */
    protected function fqnStringToClassNameScalar($string)
    {
        if (strlen($string) < 1) {
            return false;
        }

        if ($string[0] !== '\\') {
            $string = '\\' . $string;
        }

        if ($this->checkStringIsFqn($string)) {
            if(!in_array(substr($string, 1), $this->uses)) {
                return $string . '::class';
            } else {
                return substr($string, strrpos($string, '\\') + 1) . '::class';
            }
        } elseif (in_array(substr($string, 1), $this->uses)) {
            return substr($string, 1) . '::class';
        }

        return false;
    }

    /**
     * Process a string configuration key
     *
     * @param string $key
     * @return string
     */
    protected function processStringKey($key)
    {
        if ($this->useClassNameScalars && false !== ($fqnKey = $this->fqnStringToClassNameScalar($key))) {
            return $fqnKey;
        }
        if(in_array($key, $this->uses)) {
            return var_export($key, true);
        }
        return "'" . addslashes($key) . "'";
    }
}
