<?php


namespace Bitkorn\CodeCreate\Types\ModuleConfig\Elements\ServiceManager;


use Bitkorn\CodeCreate\Types\ModuleConfig\Elements\AbstractModuleConfigElement;

class ServiceManagerElement extends AbstractModuleConfigElement
{

    /**
     * ServiceManagerElement constructor.
     */
    public function __construct()
    {
        $this->configValueType = AbstractModuleConfigElement::CONFIG_VALUE_TYPE_STRING;
        $this->arrayHierarchy = ['service_manager', 'factories'];
    }

    /**
     * @param array $configValueArray
     */
    public function setConfigValueArray(array $configValueArray): void
    {
        throw new \RuntimeException('Can not set value as array in class ' . __CLASS__ . '()');
    }
}
