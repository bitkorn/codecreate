<?php


namespace Bitkorn\CodeCreate\Types\ModuleConfig\Elements\Router\Routes;


use Bitkorn\CodeCreate\Types\ModuleConfig\Elements\AbstractModuleConfigElement;

class LiteralRouteType extends AbstractRouteTypeElement
{

    /**
     * LiteralRouteType constructor.
     */
    public function __construct()
    {
        $this->configValueType = AbstractModuleConfigElement::CONFIG_VALUE_TYPE_ARRAY;
    }
}
