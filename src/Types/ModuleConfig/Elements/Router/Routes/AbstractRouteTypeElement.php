<?php


namespace Bitkorn\CodeCreate\Types\ModuleConfig\Elements\Router\Routes;


use Bitkorn\CodeCreate\Types\ModuleConfig\Elements\AbstractModuleConfigElement;

class AbstractRouteTypeElement extends AbstractModuleConfigElement
{

    /**
     * @var string
     */
    protected $routeName = '';

    /**
     * @param string $routeName
     */
    public function setRouteName(string $routeName): void
    {
        $this->routeName = $routeName;
    }

}
