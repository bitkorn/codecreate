<?php


namespace Bitkorn\CodeCreate\Types\ModuleConfig\Elements\ViewHelper;


use Bitkorn\CodeCreate\Types\ModuleConfig\Elements\AbstractModuleConfigElement;

class ViewHelperConfigElement extends AbstractModuleConfigElement
{

    /**
     * ServiceManagerElement constructor.
     */
    public function __construct()
    {
        $this->configValueType = AbstractModuleConfigElement::CONFIG_VALUE_TYPE_STRING;
        $this->arrayHierarchy = ['view_helpers', 'factories'];
    }

    /**
     * @param array $configValueArray
     */
    public function setConfigValueArray(array $configValueArray): void
    {
        throw new \RuntimeException('Can not set value as array in class ' . __CLASS__ . '()');
    }
}
