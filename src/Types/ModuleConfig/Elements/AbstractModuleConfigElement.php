<?php


namespace Bitkorn\CodeCreate\Types\ModuleConfig\Elements;


class AbstractModuleConfigElement
{
    const CONFIG_VALUE_TYPE_STRING = 1;
    const CONFIG_VALUE_TYPE_ARRAY = 2;

    /**
     * Array hierarchy including the last element.
     * E.G. ['router', 'routes'] or ['controller', 'factories']
     *
     * @var array
     */
    protected $arrayHierarchy = [];

    /**
     * @var int One of the values from AbstractModuleConfig::CONFIG_VALUE_TYPE_*
     */
    protected $configValueType = 0;

    /**
     * @var string
     */
    protected $configKey = '';

    protected $configValueString = '';
    protected $configValueArray = [];

    /**
     * @param array $arrayHierarchy
     */
    public function setArrayHierarchy(array $arrayHierarchy): void
    {
        $this->arrayHierarchy = $arrayHierarchy;
    }

    /**
     * @param string $configKey
     */
    public function setConfigKey(string $configKey): void
    {
        $this->configKey = $configKey;
    }

    /**
     * @return string
     */
    public function getConfigKey(): string
    {
        return $this->configKey;
    }

    /**
     * @param string $configValueString
     */
    public function setConfigValueString(string $configValueString): void
    {
        $this->configValueString = $configValueString;
    }

    /**
     * @return string
     */
    public function getConfigValueString(): string
    {
        return $this->configValueString;
    }

    /**
     * @param array $configValueArray
     */
    public function setConfigValueArray(array $configValueArray): void
    {
        $this->configValueArray = $configValueArray;
    }

}
