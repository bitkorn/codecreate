<?php


namespace Bitkorn\CodeCreate\Types\Functions\Controller;


use Bitkorn\CodeCreate\Types\Functions\AbstractFunction;

class AbstractControllerAction extends AbstractFunction
{

    public function __toString()
    {
        return $this->functionName . 'Action';
    }
}
