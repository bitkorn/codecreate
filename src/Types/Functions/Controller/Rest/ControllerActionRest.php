<?php


namespace Bitkorn\CodeCreate\Types\Functions\Controller\Rest;


use Bitkorn\CodeCreate\Types\Functions\Controller\AbstractControllerAction;
use Nette\PhpGenerator\Parameter;

class ControllerActionRest extends AbstractControllerAction
{

    public static $restMethods = ['GET' => 'get', 'GETlist' => 'getList', 'PUT' => 'update', 'POST' => 'create', 'DELETE' => 'delete'];

    /**
     * @var string
     */
    protected $restMethod;

    /**
     * @var
     */
    protected $functionName;

    /**
     * @var Parameter[]
     */
    protected $parameters = [];

    /**
     * @param string $restMethod One of the keys from ControllerActionRest::$restMethods
     */
    public function setRestMethod(string $restMethod): void
    {
        $restMethodKeys = array_keys(self::$restMethods);
        if (!in_array($restMethod, $restMethodKeys)) {
            throw new \RuntimeException($restMethod . ' is no valid method. Valid methods are ' . implode(', ', $restMethodKeys));
        }
        $this->restMethod = $restMethod;
        $this->functionName = self::$restMethods[$restMethod];
        switch($this->functionName) {
            case self::$restMethods['GET']:
                $paramId = new Parameter('id');
                $paramId->setType('int');
                $this->parameters[] = $paramId;
                break;
            case self::$restMethods['GETlist']:
                break;
            case self::$restMethods['PUT']:
                $paramId = new Parameter('id');
                $paramId->setType('int');
                $this->parameters[] = $paramId;
                $paramData = new Parameter('data');
                $paramData->setType('array');
                $this->parameters[] = $paramData;
                break;
            case self::$restMethods['POST']:
                $paramData = new Parameter('data');
                $paramData->setType('array');
                $this->parameters[] = $paramData;
                break;
            case self::$restMethods['DELETE']:
                $paramId = new Parameter('id');
                $paramId->setType('int');
                $this->parameters[] = $paramId;
                break;
        }
    }

    /**
     * @return Parameter[]
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    public function __toString()
    {
        return $this->functionName;
    }
}
