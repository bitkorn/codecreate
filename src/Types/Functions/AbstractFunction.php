<?php


namespace Bitkorn\CodeCreate\Types\Functions;


class AbstractFunction
{
    /**
     * @var string
     */
    protected $functionName = '';

    /**
     * AbstractFunction constructor.
     * @param string $functionName
     */
    public function __construct(string $functionName = '')
    {
        $this->functionName = $functionName;
    }

    /**
     * @param string $functionName
     */
    public function setFunctionName(string $functionName): void
    {
        $this->functionName = $functionName;
    }

    /**
     * @return mixed
     */
    public function getFunctionName()
    {
        return $this->functionName;
    }

    public function __toString()
    {
        return $this->functionName;
    }

}
