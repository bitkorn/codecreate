<?php

namespace Bitkorn\CodeCreate\Types\Functions\Entity;

use Bitkorn\CodeCreate\Types\Functions\AbstractFunction;

class Setter extends AbstractFunction
{
    public function __toString()
    {
        return 'set' . ucfirst($this->functionName);
    }

}
