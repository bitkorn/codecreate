<?php

namespace Bitkorn\CodeCreate\Types\Functions\Entity;

use Bitkorn\CodeCreate\Types\Functions\AbstractFunction;

class Getter extends AbstractFunction
{
    public function __toString()
    {
        return 'get' . ucfirst($this->functionName);
    }

}
