<?php

namespace Bitkorn\CodeCreate;

use Bitkorn\CodeCreate\Controller\Ajax\Common\ComposerAutoloadController;
use Bitkorn\CodeCreate\Controller\Ajax\Common\FilesystemAutoloadController;
use Bitkorn\CodeCreate\Controller\Ajax\Common\TablenameAutoloadController;
use Bitkorn\CodeCreate\Controller\Create\BasicController;
use Bitkorn\CodeCreate\Controller\Create\EntityController;
use Bitkorn\CodeCreate\Controller\Trial\Netteway\NettewayController;
use Bitkorn\CodeCreate\Controller\Trial\TestController;
use Bitkorn\CodeCreate\Controller\Trial\Zendway\ZendwayController;
use Bitkorn\CodeCreate\Controller\TrinketController;
use Bitkorn\CodeCreate\Factory\Controller\Ajax\Common\ComposerAutoloadControllerFactory;
use Bitkorn\CodeCreate\Factory\Controller\Ajax\Common\FilesystemAutoloadControllerFactory;
use Bitkorn\CodeCreate\Factory\Controller\Ajax\Common\TablenameAutoloadControllerFactory;
use Bitkorn\CodeCreate\Factory\Controller\Create\BasicControllerFactory;
use Bitkorn\CodeCreate\Factory\Controller\Create\EntityControllerFactory;
use Bitkorn\CodeCreate\Factory\Controller\Trial\Netteway\NettewayControllerFactory;
use Bitkorn\CodeCreate\Factory\Controller\Trial\TestControllerFactory;
use Bitkorn\CodeCreate\Factory\Controller\Trial\Zendway\ZendwayControllerFactory;
use Bitkorn\CodeCreate\Factory\Controller\TrinketControllerFactory;
use Bitkorn\CodeCreate\Factory\Form\Create\BasicFormFactory;
use Bitkorn\CodeCreate\Factory\Form\Create\ModuleFormFactory;
use Bitkorn\CodeCreate\Factory\Service\Create\File\ConfigFileServiceFactory;
use Bitkorn\CodeCreate\Factory\Service\Create\Module\CreateModuleServiceFactory;
use Bitkorn\CodeCreate\Factory\Service\Create\Type\CreateTypeServiceFactory;
use Bitkorn\CodeCreate\Factory\Service\Create\Type\MappingGetterSetterServiceFactory;
use Bitkorn\CodeCreate\Form\Create\BasicForm;
use Bitkorn\CodeCreate\Form\Create\ModuleForm;
use Bitkorn\CodeCreate\Service\Create\File\ConfigFileService;
use Bitkorn\CodeCreate\Service\Create\Module\CreateModuleService;
use Bitkorn\CodeCreate\Service\Create\Type\CreateTypeService;
use Bitkorn\CodeCreate\Service\Create\Type\MappingGetterSetterService;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            /*
             * Basic
             */
            'bitkorn_codecreate_create_basic_createtypes' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/bitkorn-codecreate-basic-createtypes',
                    'defaults' => [
                        'controller' => BasicController::class,
                        'action' => 'createTypes',
                    ],
                ],
            ],
            'bitkorn_codecreate_create_basic_createmodule' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/bitkorn-codecreate-basic-createmodule',
                    'defaults' => [
                        'controller' => BasicController::class,
                        'action' => 'createModule',
                    ],
                ],
            ],
            /*
             * Entity
             */
            'bitkorn_codecreate_create_entity_createentity' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/bitkorn-codecreate-entity-createentity',
                    'defaults' => [
                        'controller' => EntityController::class,
                        'action' => 'createEntity',
                    ],
                ],
            ],
            /*
             * AJAX
             */
            'bitkorn_codecreate_ajax_common_composerautoload_autocompletenamespace' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/bitkorn-codecreate-ajax-common-autocomplete-namespace',
                    'defaults' => [
                        'controller' => ComposerAutoloadController::class,
                        'action' => 'autocompleteNamespace',
                    ],
                ],
            ],
            'bitkorn_codecreate_ajax_common_filesystemautoload_autocompletefilesystem' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/bitkorn-codecreate-ajax-common-autocomplete-filesystem',
                    'defaults' => [
                        'controller' => FilesystemAutoloadController::class,
                        'action' => 'autocompleteFilesystem',
                    ],
                ],
            ],
            'bitkorn_codecreate_ajax_common_tablenameautoload_autocompletetablename' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/bitkorn-codecreate-ajax-common-autocomplete-tablename',
                    'defaults' => [
                        'controller' => TablenameAutoloadController::class,
                        'action' => 'autocompleteTablename',
                    ],
                ],
            ],
            /**
             * trinket
             */
            'bitkorn_codecreate_trinket_dbtablemapping' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/codecreate-db-table-mapping',
                    'defaults' => [
                        'controller' => TrinketController::class,
                        'action' => 'dbTableMapping',
                    ],
                ],
            ],
            'bitkorn_codecreate_trinket_dbtablemapping_postgresql' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/codecreate-db-table-mapping-pg',
                    'defaults' => [
                        'controller' => TrinketController::class,
                        'action' => 'dbTableMappingPostgresql',
                    ],
                ],
            ],
            /*
             * Trial
             */
            'bitkorn_codecreate_trial_zendway' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/bitkorn-codecreate-trial-zendway',
                    'defaults' => [
                        'controller' => ZendwayController::class,
                        'action' => 'doit',
                    ],
                ],
            ],
            'bitkorn_codecreate_trial_netteway' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/bitkorn-codecreate-trial-netteway',
                    'defaults' => [
                        'controller' => NettewayController::class,
                        'action' => 'doit',
                    ],
                ],
            ],
            'bitkorn_codecreate_trial_test_routetoconfigzend' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/bitkorn-codecreate-trial-test-routetoconfigzend',
                    'defaults' => [
                        'controller' => TestController::class,
                        'action' => 'routeToConfigZend',
                    ],
                ],
            ],
            'bitkorn_codecreate_trial_test_routetoconfignette' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/bitkorn-codecreate-trial-test-routetoconfignette',
                    'defaults' => [
                        'controller' => TestController::class,
                        'action' => 'routeToConfigNette',
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            // create
            BasicController::class => BasicControllerFactory::class,
            EntityController::class => EntityControllerFactory::class,
            // common
            ComposerAutoloadController::class => ComposerAutoloadControllerFactory::class,
            FilesystemAutoloadController::class => FilesystemAutoloadControllerFactory::class,
            TablenameAutoloadController::class => TablenameAutoloadControllerFactory::class,
            //
            TrinketController::class => TrinketControllerFactory::class,
            // Trial
            ZendwayController::class => ZendwayControllerFactory::class,
            NettewayController::class => NettewayControllerFactory::class,
            TestController::class => TestControllerFactory::class,
        ],
    ],
    'service_manager' => [
        'invokables' => [
        ],
        'factories' => [
            // Service
            CreateTypeService::class => CreateTypeServiceFactory::class,
            ConfigFileService::class => ConfigFileServiceFactory::class,
            CreateModuleService::class => CreateModuleServiceFactory::class,
            MappingGetterSetterService::class => MappingGetterSetterServiceFactory::class,
            // Form
            BasicForm::class => BasicFormFactory::class,
            ModuleForm::class => ModuleFormFactory::class,
        ]
    ],
    'view_helpers' => [
        'invokables' => [
        ]
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'template_map' => [
            'layout/layout-code-create'           => __DIR__ . '/../view/layout/layout.phtml',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
    'bitkorn_codecreate' => [
        'autoload_psr4' => __DIR__ . '/../../../../vendor/composer/autoload_psr4.php',
        'module_root' => realpath(__DIR__ . '/../../..'),
        'draft_folder' => realpath(__DIR__ . '/../data/draft'),
        'supported_types' => [
            'controller_html' => [
                'desc' => 'HTML Controller with Action and Factory (ViewModel returned)',
                'type_foldername' => 'Controller'
            ],
            'controller_ajax' => [
                'desc' => 'JSON Controller with Action and Factory (JsonModel returned)',
                'type_foldername' => 'Controller'
            ],
            'controller_rest' => [
                'desc' => 'REST Controller with Factory and Actions for GET, GET list, POST, PUT, DELETE',
                'type_foldername' => 'Controller'
            ],
            'table' => [
                'desc' => 'Table with Factory (adapter name = dbDefault)',
                'type_foldername' => 'Table'
            ],
            'form' => [
                'desc' => 'Form with Factory',
                'type_foldername' => 'Form'
            ],
            'service' => [
                'desc' => 'Service with Factory',
                'type_foldername' => 'Service'
            ],
            'view_helper' => [
                'desc' => 'ViewHelper with Factory',
                'type_foldername' => 'View'
            ],
            'command' => [
                'desc' => 'Command with Factory',
                'type_foldername' => 'Command'
            ],
        ],
    ]
];
